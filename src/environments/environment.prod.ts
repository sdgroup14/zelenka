export const environment = {
  production: true,
  apiUrl: 'https://api.iod.media',
  domain: 'https://iod.media'
};
