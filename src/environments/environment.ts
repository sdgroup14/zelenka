export const environment = {
  production: false,
  apiUrl: 'https://api.iod.media',
  domain: 'https://iod.media'
};

