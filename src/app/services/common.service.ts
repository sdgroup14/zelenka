import {Injectable} from '@angular/core';
import {CookiesService} from '@ngx-utils/cookies';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {BehaviorSubject, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public url = new BehaviorSubject(null);
  public notif = new Subject();
  public login = new Subject();
  backForwardState = new BehaviorSubject(false);

  constructor(private cookie: CookiesService, private translate: TranslateService, private router: Router, private http: HttpClient) {
  }

  public langPath() {
    const path = '';
    const lang = this.cookie.get('applang');
    if (lang) {
      if (lang === 'uk') {
        return '';
      }
      return '/' + lang;
    } else {
      const url = this.router.url.split('/')[1];
      return url === 'ru' ? '/' + url : '';
    }
    return path;
  }


  copyText(val, query?) {
    if (typeof val === 'boolean') {
      const url = window.location.href;
      const urlArray = url.split('?');
      // console.log(urlArray);
      if (urlArray.length > 1) {
        val = urlArray[0];
      } else {
        val = url;
      }
    }
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    if (query) {
      selBox.value = val + '?' + query;
    }
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  debugHttp(params) {
    return this.http.post(environment.apiUrl + '/debug/log404', {...params}).subscribe(resp => resp);
  }


}
