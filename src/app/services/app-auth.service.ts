import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {PlatformService} from './platform.service';
import {CookiesService} from '@ngx-utils/cookies';
import {CommonService} from './common.service';
import {TranslateService} from '@ngx-translate/core';
// import * as firebase from 'firebase/app';
// import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AppAuthService {
  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient,
              // public  afAuth: AngularFireAuth,
              private translate: TranslateService,
              public _common: CommonService,
              private router: Router,
              private platform: PlatformService,
              private cookie: CookiesService) {
    if (this.platform.check()) {
      if (this.cookie.get('appToken')) {
        this.currentUserSubject = new BehaviorSubject(JSON.parse(this.cookie.get('appToken')));
      } else {
        this.currentUserSubject = new BehaviorSubject(null);
      }
    } else {
      this.currentUserSubject = new BehaviorSubject(null);
    }
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  public refreshToken() {
    const apiOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json')
    };
    const _refresh = this.currentUserValue.refresh;
    const _sign = this.currentUserValue.sign;
    this.currentUserSubject.next(null);
    return this.http.post<any>(`${environment.apiUrl}/auth/refresh`, JSON.stringify({refresh_token: _refresh, sign: _sign}), apiOptions);
  }

  loginWithSocial(token: string, provider: string, secret?: string) {
    const params = secret ? '?oauth_token=' + token + '&oauth_verifier=' + secret : '?token=' + token;
    return this.http.post<any>(environment.apiUrl + '/auth/' + provider + params, JSON.stringify({}))
      .subscribe((resp: any) => {
        if (resp) {
          this.translate.get('loginModal.notif').subscribe(text => {
            this._common.notif.next(text + resp.user.name);

            setTimeout(() => {
              this._common.notif.next(null);
            }, 4000);
          });
          this._common.login.next(false);
          // const _d = new Date();
          // 5000
          const d_final = new Date(resp.refresh_exp * 1000);
          // const d_final = new Date(Date.now() + 5000);
          this.cookie.put('appToken', JSON.stringify(resp), {
            path: '/',
            expires: d_final
          });
          this.currentUserSubject.next(resp);
          if (provider === 'email') {
            this.router.navigate(['/']);
          }
          if (localStorage.getItem('appRedirect')) {
            this.router.navigate([localStorage.getItem('appRedirect')]);
            localStorage.removeItem('appRedirect');
            localStorage.removeItem('authProvider');
          }
        }

        return resp.result;
      });
  }

  // loginWithSocial(token: string, provider: string, secret?: string) {
  //   const params = secret ? '?oauth_token=' + token + '&oauth_verifier=' + secret : '?token=' + token;
  //   return this.http.post<any>(environment.GETApiUrl + '/auth/' + provider + params, JSON.stringify({}))
  //     .subscribe((data: any) => {
  //       const resp = data.result;
  //       if (resp) {
  //         this.translate.get('notification-modal.login-success-text').subscribe(text => {
  //           this._common.notification.next({
  //             type: 'succsess',
  //             text: text + resp.user.name
  //           });
  //         });
  //         this._common.login.next(false);
  //         const d_final = new Date(resp.refresh_exp * 1000);
  //         this.cookie.put('appToken', JSON.stringify(resp), {
  //           expires: d_final
  //         });
  //         this.currentUserSubject.next(resp);
  //         if (provider === 'email') {
  //           this.router.navigate(['/']);
  //         }
  //         // console.log(localStorage.getItem('appRedirect'))
  //         if (localStorage.getItem('appRedirect')) {
  //           this.router.navigate([localStorage.getItem('appRedirect')]);
  //           localStorage.removeItem('appRedirect');
  //           localStorage.removeItem('authProvider');
  //         }
  //       }
  //
  //       return resp.result;
  //     });
  // }

  loginWithEmail(email) {
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json')
    };

    return this.http.post(environment.apiUrl + '/auth/send-email', {email}, apiOptions);
  }

  getUser() {
    const token = this.currentUserValue.token;
    const apiOptions = {
      headers: new HttpHeaders().set('Authorization', token)
    };
    return this.http.get(environment.apiUrl + '/user', apiOptions);
  }

  authTwitter() {
    return this.http.get(environment.apiUrl + '/auth/twitter-oauth-token', {});
  }

  public clear() {
    this.cookie.remove('appToken');
    this.currentUserSubject.next(null);
    this.router.navigate(['/']);
    // this.afAuth.auth.signOut().then(() => {
    //   console.log('logout');
    // });
  }

  logout() {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', this.currentUserValue.token)
    };
    return this.http.post<any>(
      `${environment.apiUrl}/auth/logout`,
      {sign: this.currentUserValue.sign, refresh: this.currentUserValue.refresh},
      apiOptions
    ).subscribe(resp => {
      this.clear();
    });

  }
}
