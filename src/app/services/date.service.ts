import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {CookiesService} from '@ngx-utils/cookies';
import {NavigationEnd, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  convert(timestamp) {
    return moment(timestamp * 1000).format('ll');
  }

  convertWithTime(timestamp) {
    if (timestamp) {
      const bdDate = new Date(timestamp * 1000);
      return moment.utc(bdDate).format('lll');
    }
  }

  convertWithTimeUserTimezone(timestamp) {
    if (timestamp) {
      const bdDate = new Date(timestamp * 1000);
      return moment(bdDate).format('lll');
    }
  }



  constructor(private cookie: CookiesService, private router: Router) {
    const lang = this.cookie.get('applang');
    if (lang) {
      moment.locale(lang);
    } else {
      this.router.events.subscribe((event: any) => {
        if (event instanceof NavigationEnd) {
          const routerLang: any = event.url.split('/')[1];
          const _lang = routerLang === 'ru' ? 'ru' : 'uk';
          moment.locale(_lang);
        }
      });
    }
  }
}
