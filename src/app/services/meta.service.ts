import {Inject, Injectable} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';
import {CookiesService} from '@ngx-utils/cookies';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {PlatformService} from './platform.service';
import * as moment from 'moment';
import {CommonService} from './common.service';

declare var _io_config: any;

@Injectable({
  providedIn: 'root'
})
export class MetaService {
  script: any;

  constructor(private meta: Meta,
              private platform: PlatformService,
              private titleService: Title,
              private cookie: CookiesService,
              private common: CommonService,
              private router: Router,
              // private renderer2: Renderer2,
              @Inject(DOCUMENT) private doc) {

    this.script = this.doc.createElement('script');
  }

  changeIoArticle(url, title, pagetype, author, tag, publish_time, count) {
    let lang = this.cookie.get('applang');
    if (!lang) {
      const checkRU = this.router.url.split('/')[1];
      lang = checkRU === 'ru' ? 'ru' : 'uk';
    }
    const escapeHtml = (unsafe) => {
      return unsafe
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#039;');
    };
    let canonicalUrl1 = environment.domain + this.router.url;
    if (canonicalUrl1.indexOf('?') > 0) {
      canonicalUrl1 = environment.domain + this.router.url.split('?')[0];
    }

    // if (_io_config) {
    //   // setTimeout(() => {
    //   //   _io_config.push({
    //   //     page_url: environment.domain + url,
    //   //     page_url_canonical: canonicalUrl1,
    //   //     page_title: escapeHtml(title),
    //   //     page_type: pagetype,
    //   //     page_language: lang,
    //   //     article_authors: [author],
    //   //     article_categories: [tag],
    //   //     article_word_count: count,
    //   //     article_type: 'longread',
    //   //     article_publication_date: moment(publish_time * 1000).locale('en').format('ddd, DD MMM YYYY HH:mm:ss ZZ')
    //   //   });
    //   // }, 3000);
    //
    // } else {
    //
    // }
    this.script.text = `
          window._io_config = window._io_config || {};
      window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
      window._io_config["0.2.0"].push({
        page_url: "` + environment.domain + url + `",
        page_url_canonical: "` + canonicalUrl1 + `",
        page_title: "` + escapeHtml(title) + `",
        page_type: "` + pagetype + `",
        page_language: "` + lang + `",
        article_authors: ["` + author + `"],
        article_categories: ["` + tag + `"],
        article_word_count: "` + count + `",
        article_type: "longread",
        article_publication_date: "` + moment(publish_time * 1000).locale('en').format('ddd, DD MMM YYYY HH:mm:ss ZZ') + `"
      });
      `;
    // this.common.ioSend()
    //   .subscribe(resp => {
    //     console.log(resp);
    //   });    // console.log(io);

    // this.
    // console.log(window._io_config);

  }

  setMeta(title, title2, description, keywords, img, url, pagetype, data?) {
    let lang = this.cookie.get('applang');
    if (!lang) {
      const checkRU = this.router.url.split('/')[1];
      lang = checkRU === 'ru' ? 'ru' : 'uk';
    }
    this.titleService.setTitle(title);

    const link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'canonical');
    const robotsContent = 'index, follow';
    const canonicalUrl = environment.domain + this.router.url;
    if (canonicalUrl.indexOf('?') > 0) {
      // robotsContent = 'noindex, follow';
      // canonicalUrl = environment.domain + this.router.url.split('?')[0];
    }
    link.setAttribute('href', canonicalUrl);
    if (!this.platform.check()) {
      this.doc.head.appendChild(link);
    }

    this.meta.addTag({name: 'description', content: description});
    this.meta.addTag({name: 'keywords', content: keywords});
    this.meta.addTag({name: 'image_src', content: img});

    this.meta.addTag({name: 'robots', content: robotsContent});

    this.meta.addTag({name: 'twitter:site', content: '@IOD_MEDIA'});
    this.meta.addTag({name: 'twitter:card', content: 'summary_large_image'});

    this.meta.addTag({name: 'twitter:title', content: title2});
    this.meta.addTag({name: 'twitter:description', content: description});
    this.meta.addTag({name: 'twitter:creator', content: '@IOD_MEDIA'});
    this.meta.addTag({name: 'twitter:image', content: img});
    this.meta.addTag({name: 'twitter:url', content: environment.domain + url});

    this.meta.addTag({property: 'og:type', content: 'website'});
    this.meta.addTag({property: 'og:title', content: title2});
    this.meta.addTag({property: 'og:description', content: description});
    this.meta.addTag({property: 'og:url', content: environment.domain + url});
    // console.log(lang)
    this.meta.addTag({property: 'og:locale', content: lang + '_' + (lang === 'ru' ? 'ru' : 'ua').toUpperCase()});
    this.meta.addTag({property: 'og:site_name', content: 'ЙОД.MEDIA'});
    this.meta.addTag({property: 'og:image', content: img});
    this.meta.addTag({property: 'og:image:type', content: 'image/jpeg'});
    this.meta.addTag({property: 'og:image:width', content: '1200'});
    this.meta.addTag({property: 'og:image:height', content: '630'});
    this.meta.addTag({property: 'og:image:secure_url', content: img});
    this.meta.addTag({property: 'fb:app_id', content: '413681322735717'});


    const escapeHtml = (unsafe) => {
      return unsafe
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#039;');
    };

    let canonicalUrl1 = environment.domain + this.router.url;
    if (canonicalUrl1.indexOf('?') > 0) {
      canonicalUrl1 = environment.domain + this.router.url.split('?')[0];
    }
    // console.log(window._io_config)
    // window._io_config = {};
    // if (!this.platform.check()) {
    // const s = this.doc.createElement('script');
    this.script.type = 'text/javascript';

    if (pagetype === 'article') {
      this.changeIoArticle(url, title, pagetype, data.author, data.tag, data.publish_time, 0);
    } else if (pagetype === 'default') {


      // if (_io_config) {
      //   _io_config.push({
      //     page_url: environment.domain + url,
      //     page_url_canonical: canonicalUrl1,
      //     page_title: escapeHtml(title),
      //     page_type: pagetype,
      //     page_language: lang,
      //     article_authors: [author],
      //     article_categories: [tag],
      //     article_word_count: count,
      //     article_type: 'longread',
      //     article_publication_date: moment(publish_time * 1000).locale('en').format('ddd, DD MMM YYYY HH:mm:ss ZZ')
      //   });
      // } else {
      //   this.script.text = `
      //     window._io_config = window._io_config || {};
      // window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
      // window._io_config["0.2.0"].push({
      //   page_url: "` + environment.domain + url + `",
      //   page_url_canonical: "` + canonicalUrl1 + `",
      //   page_title: "` + escapeHtml(title) + `",
      //   page_type: "` + pagetype + `",
      //   page_language: "` + lang + `",
      //   article_authors: ["` + author + `"],
      //   article_categories: ["` + tag + `"],
      //   article_word_count: "` + count + `",
      //   article_type: "longread",
      //   article_publication_date: "` + moment(publish_time * 1000).locale('en').format('ddd, DD MMM YYYY HH:mm:ss ZZ') + `"
      // });
      // `;
      // }

      this.script.text = `
       window._io_config = window._io_config || {};
        window._io_config['0.2.0'] = window._io_config['0.2.0'] || [];
        window._io_config['0.2.0'].push({
          page_url: '` + environment.domain + url + `',
          page_url_canonical:  "` + canonicalUrl1 + `",
          page_title: "` + escapeHtml(title) + `",
          page_type: 'default',
          page_language: '` + lang + `'
        });
      `;

    } else if (pagetype === 'main') {
      this.script.text = `
          window._io_config = window._io_config || {};
        window._io_config['0.2.0'] = window._io_config['0.2.0'] || [];
        window._io_config['0.2.0'].push({
          page_url: '` + environment.domain + url + `',
          page_url_canonical: '` + canonicalUrl1 + `',
          page_title: "` + escapeHtml(title) + `",
          page_type: 'main',
          page_language: '` + lang + `'
        });
      `;
    }

    this.doc.body.appendChild(this.script);
    // }
  }

}
