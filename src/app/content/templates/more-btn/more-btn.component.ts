import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-more-btn',
  templateUrl: './more-btn.component.html',
  styleUrls: ['./more-btn.component.scss']
})
export class MoreBtnComponent implements OnInit {
  @Input('textLeft') textLeft: string;
  @Input('textRight') textRight: string;
  constructor() { }

  ngOnInit() {
  }

}
