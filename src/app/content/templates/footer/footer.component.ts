import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MenuService} from '../menu/menu.service';
import {Subscription} from 'rxjs';
import {CommonService} from '../../../services/common.service';
import {CookiesService} from '@ngx-utils/cookies';
import {NavigationEnd, Router} from '@angular/router';
import {PlatformService} from '../../../services/platform.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscriptions = new Subscription();
  tags = [];
  lang = true;
  @ViewChild('liveEl', { static: false }) liveEl: ElementRef;
  @ViewChild('bigmirEl', { static: false }) bigmirEl: ElementRef;

  changeLang(lang) {
    if (this.cookie.get('applang') === lang) {
      return;
    }
    this.cookie.remove('applang');
    const _d = new Date();
    const exp = _d.setFullYear(_d.getFullYear() + 1);
    const d_final = new Date(exp);

    this.cookie.put('applang', lang, {

      path: '/',
      expires: d_final
    });
    window.location.reload();
  }

  constructor(private service: MenuService,
              private platform: PlatformService,
              public _common: CommonService,
              private router: Router,
              private cookie: CookiesService) {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngAfterViewInit(): void {
    // if (this.platform.check()) {
    //   const element = this.liveEl.nativeElement;
    //   const script = document.createElement('script');
    //   script.type = 'text/javascript';
    //   element.innerHTML = '<a href=\'//www.liveinternet.ru/click\' ' +
    //     'target=_blank><img src=\'//counter.yadro.ru/hit?t44.6;r' +
    //     escape(document.referrer) + ((typeof (screen) === 'undefined') ? '' :
    //       ';s' + screen.width + '*' + screen.height + '*' + (screen.colorDepth ?
    //       screen.colorDepth : screen.pixelDepth)) + ';u' + escape(document.URL) +
    //     ';h' + escape(document.title.substring(0, 150)) + ';' + Math.random() +
    //     '\' alt=\'\' title=\'LiveInternet\' ' +
    //     'border=\'0\' width=\'22\' height=\'22\'></a>';
    // }
  }

  ngOnInit() {
    const lang = this.cookie.get('applang');
    if (lang) {
      this.lang = lang === 'uk';
    } else {
      this.router.events.subscribe((event: any) => {
        if (event instanceof NavigationEnd) {
          const routerLang: any = event.url.split('/')[1];
          this.lang = routerLang === 'ru' ? false : true;
        }
      });

    }
    this.subscriptions.add(
      this.service.getMenu().subscribe((resp: any) => {
        if (resp) {
          this.tags = resp.full;
        }
      })
    );
  }

}
