import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-notif-modal',
  templateUrl: './notif-modal.component.html',
  styleUrls: ['./notif-modal.component.scss']
})
export class NotifModalComponent implements OnInit {
  @Input() txt: string;
  @Output() hide = new EventEmitter();

  constructor() {
  }

  ngOnInit() {}

}
