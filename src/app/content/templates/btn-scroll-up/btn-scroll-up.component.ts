import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-scroll-up',
  templateUrl: './btn-scroll-up.component.html',
  styleUrls: ['./btn-scroll-up.component.scss']
})
export class BtnScrollUpComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  scrollUp() {
    document.body.scrollIntoView({behavior: 'smooth', block: 'start'});
  }

}
