import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appBtnScrollVisibility]'
})
export class BtnScrollVisibilityDirective {

  @HostListener('window:scroll', ['$event'])
  onScroll(e) {
    if (e.target.scrollingElement.scrollTop > 150) {
      this.renderer.addClass(this.element.nativeElement, '__show')
    } else {
      this.renderer.removeClass(this.element.nativeElement, '__show')
    }

  }

  constructor(private element: ElementRef, private renderer: Renderer2) {
  }

}
