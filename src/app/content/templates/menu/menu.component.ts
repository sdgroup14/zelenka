import {Component, EventEmitter, OnDestroy, OnInit, Output, Renderer2} from '@angular/core';
import {Subscription} from 'rxjs';
import {MenuService} from './menu.service';
import {CommonService} from '../../../services/common.service';
import {CookiesService} from '@ngx-utils/cookies';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @Output() hide = new EventEmitter();
  tags = [];
  color = '#ffffff';

  langs = [
    {
      text: 'укр',
      value: 'uk',
      selected: false
    },
    {
      text: 'рус',
      value: 'ru',
      selected: true
    }
  ];

  changePage() {
    this._renderer.removeClass(document.body, 'scrollOff');
    this.hide.emit();
  }

  changeLang(lang) {
    if (this.cookie.get('applang') === lang) {
      return;
    }
    this.cookie.remove('applang');
    const _d = new Date();
    const exp = _d.setFullYear(_d.getFullYear() + 1);
    const d_final = new Date(exp);
    this.cookie.put('applang', lang, {
      path: '/',
      expires: d_final
    });
    window.location.reload();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: MenuService, private router: Router, private cookie: CookiesService, private _renderer: Renderer2, public _common: CommonService) {
  }


  ngOnInit() {
    const lang = this.cookie.get('applang');

    if (lang) {
      if (lang === 'ru') {
        this.langs[0].selected = false;
        this.langs[1].selected = true;
      } else {
        this.langs[0].selected = true;
        this.langs[1].selected = false;
      }
    } else {
      this.router.events.subscribe((event: any) => {
        if (event instanceof NavigationEnd) {
          const routerLang: any = event.url.split('/')[1];
          if (routerLang === 'ru') {
            this.langs[0].selected = false;
            this.langs[1].selected = true;
          } else {
            this.langs[0].selected = true;
            this.langs[1].selected = false;
          }
        }
      });
    }

    this.subscriptions.add(
      this.service.getMenu().subscribe((resp: any) => {
        if (resp) {
          this.color = resp.color;
          let idx = 0;
          resp.full.map(tag => {
            idx++;
            if (idx === 1) {
              tag.size = 25;
            } else if (idx === 2) {
              tag.size = 40;
            } else if (idx === 3) {
              tag.size = 30;
            } else if (idx === 4) {
              tag.size = 45;
            } else if (idx === 5) {
              tag.size = 35;
              idx = 0;
            }

          });
          this.tags = resp.full;
        }
      })
    );
  }

}
