import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public menu = new BehaviorSubject(null);

  changeMenu(data): void {
    this.menu.next(data);
  }

  getMenu(): Observable<any> {
    return this.menu.asObservable();
  }

  constructor(private http: HttpClient) {
    this.getMenuApi();
  }

  getMenuApi() {
    return this.http.get(environment.apiUrl + '/menu', {}).subscribe(resp => {
      this.changeMenu(resp);
    });
  }
}
