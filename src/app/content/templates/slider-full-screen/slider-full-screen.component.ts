import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {GalleryService} from "../gallery/gallery.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CommonService} from "../../../services/common.service";
import {MetaService} from "../../../services/meta.service";
import {PlatformService} from "../../../services/platform.service";
import Swiper from "swiper";
import {select, Store} from "@ngrx/store";
import {selectFullScreenSliderCaption, selectFullScreenSliderItems} from "../../../store/selectors/article.selector";
import {IAppState} from "../../../store/state/app.state";
import {showSliderModal} from "../../../store/actions/article.actions";

@Component({
  selector: 'app-slider-full-screen',
  templateUrl: './slider-full-screen.component.html',
  styleUrls: ['./slider-full-screen.component.scss']
})
export class SliderFullScreenComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscriptions = new Subscription();
  slides$ = this.store.pipe(select(selectFullScreenSliderItems))
  caption$ = this.store.pipe(select(selectFullScreenSliderCaption))
  // index: any;
  // shareIsOpen = false;
  sW = 0;
  sH = 0;
  timeout: any;
  // link = '/caricatures';
  swiper: any;
  @ViewChild('wrapEl', {static: true}) wrapEl: ElementRef;
  // caricature: any = {};
  // slides = [];
  // init = false;
  // direction: any = true;
  // checkExist = false;
  // share: any = {
  //   fb: '',
  //   tw: '',
  //   tm: '',
  //   vb: ''
  // };

  constructor(
    private _renderer: Renderer2,
    private service: GalleryService,
    private store: Store<IAppState>,
    private router: Router,
    public _common: CommonService,
    private sMeta: MetaService,
    private platform: PlatformService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    if (this.platform.check()) {
      this._renderer.addClass(document.body, 'scrollOff');
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.service.change(null);
    if (this.platform.check()) {
      this._renderer.removeClass(document.body, 'scrollOff');
    }
  }

  close() {
    this.store.dispatch(showSliderModal({value: false, slides: [], caption: ''}))
  }

  @HostListener('window:resize', ['$event'])
  onResize(time?) {
    this.sW = this.sH = this.wrapEl.nativeElement.offsetHeight;
    time = time || 1000;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.swiper.update();
    }, 100);
  }

  changeSlideHandler(direction) {
    this.swiper.slideTo(this.swiper.activeIndex + (direction ? 1 : -1));
  }

  //
  //
  // changeCaricature(next) {
  //   this.checkExist = false;
  //   this.direction = next;
  //   const key = this.direction ? 'next' : 'prev';
  //   const nextSlug = this.slides[this.swiper.activeIndex][key];
  //   if (nextSlug) {
  //     const exist = this.slides.filter(slide => {
  //       return slide.slug === nextSlug;
  //     })[0];
  //     this.router.navigate([this._common.langPath() + '/caricature', nextSlug]);
  //     if (exist) {
  //       this.checkExist = true;
  //       this.share.fb = 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(exist.slug);
  //       this.share.tw = 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(exist.slug);
  //       this.share.tm = 'https://telegram.me/share/url?url=' + this.encodeHandler(exist.slug);
  //       this.share.vb = 'viber://forward?text=' + this.encodeHandler(exist.slug);
  //       this.caricature.current = exist.current;
  //       this.caricature.caption = exist.caption;
  //       this.caricature.total = exist.total;
  //       this.changeSlideHandler(exist.slug);
  //     }
  //   }
  // }

  // openWindow(link) {
  //   const w = 560;
  //   const h = 640;
  //   const left = (screen.width / 2) - (w / 2);
  //   const top = (screen.height / 2) - (h / 2);
  //   return window.open(link, 'window', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  // }
  //
  // encodeHandler(href) {
  //   return environment.domain + this._common.langPath() + '/caricature/' + encodeURI(href);
  // }

  ngAfterViewInit() {
    if (this.platform.check()) {
      this.swiper = new Swiper(this.wrapEl.nativeElement, {
        direction: 'horizontal',
        // autoHeight: true,
        observer: true,
        speed: 1,
        loop: false,
        slidesPerView: 1,
        // shortSwipes: false,
        // longSwipes: false,
        // longSwipesRatio: 25,
        // followFinger: false,
        // touchEventsTarget: 'wrapper'
      });
      this.onResize()
      setTimeout(() => {
        this.swiper.update();
      }, 1100)
    }
  }

}
