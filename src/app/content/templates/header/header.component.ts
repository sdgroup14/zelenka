import {Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, Renderer2} from '@angular/core';
import {Subscription} from 'rxjs';
import {MenuService} from '../menu/menu.service';
import {CommonService} from '../../../services/common.service';
import {CookiesService} from '@ngx-utils/cookies';
import {NavigationEnd, Router} from '@angular/router';
import {AppAuthService} from '../../../services/app-auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @Output() menuCheck = new EventEmitter();
  @Output() searchCheck = new EventEmitter();
  @Output() authCheck = new EventEmitter();
  @Input() menuIsOpen: any;
  @Input() searchIsOpen: any;
  @Input() authIsOpen: any;
  tags: any = [];
  lang = true;
  checkLangCookie = false;
  isLogin = false;
  user: any = {};

  constructor(private _renderer: Renderer2,
              private zone: NgZone,
              public _common: CommonService,
              private router: Router,
              private appAuth: AppAuthService,
              private service: MenuService,
              private cookie: CookiesService) {
  }

  menuToggle() {
    this.searchIsOpen = false;
    this.searchCheck.emit(this.searchIsOpen);
    this.authIsOpen = false;
    this.authCheck.emit(this.authIsOpen);

    this.menuIsOpen = !this.menuIsOpen;
    this.menuIsOpen ? this._renderer.addClass(document.body, 'scrollOff') : this._renderer.removeClass(document.body, 'scrollOff');
    this.menuCheck.emit(this.menuIsOpen);
  }

  searchToggle() {
    this.menuIsOpen = false;
    this.menuCheck.emit(this.menuIsOpen);
    this.authIsOpen = false;
    this.authCheck.emit(this.authIsOpen);


    this.searchIsOpen = !this.searchIsOpen;
    this.searchIsOpen ? this._renderer.addClass(document.body, 'scrollOff') : this._renderer.removeClass(document.body, 'scrollOff');
    this.searchCheck.emit(this.searchIsOpen);
  }

  authToggle() {
    this.menuIsOpen = false;
    this.menuCheck.emit(this.menuIsOpen);
    this.searchIsOpen = false;
    this.searchCheck.emit(this.searchIsOpen);

    this.authIsOpen = !this.authIsOpen;
    this.authIsOpen ? this._renderer.addClass(document.body, 'scrollOff') : this._renderer.removeClass(document.body, 'scrollOff');
    this.authCheck.emit(this.authIsOpen);
  }

  closeAll() {
    this.authIsOpen = false;
    this.authCheck.emit(this.authIsOpen);
    this.menuIsOpen = false;
    this.menuCheck.emit(this.menuIsOpen);
    this.searchIsOpen = false;
    this.searchCheck.emit(this.searchIsOpen);
  }


  changeLang(lang) {
    if (this.cookie.get('applang') === lang) {
      return;
    }
    this.cookie.remove('applang');
    const _d = new Date();
    const exp = _d.setFullYear(_d.getFullYear() + 1);
    const d_final = new Date(exp);
    this.cookie.put('applang', lang, {
      path: '/',
      expires: d_final
    });
    window.location.reload();
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.appAuth.currentUserSubject.subscribe(data => {
      if (data) {
        this.zone.run(() => {
          this.isLogin = true;
          this.user = data.user;
          this.authCheck.emit(false);
          this._renderer.removeClass(document.body, 'scrollOff');
        });
      } else {
        this.isLogin = false;
        this.user = null;
      }
    });

    if (this.appAuth.currentUserValue) {
      this.isLogin = true;
      this.user = this.appAuth.currentUserValue.user;
    }
    const lang = this.cookie.get('applang');

    if (lang) {
      this.lang = lang === 'uk';
      this.checkLangCookie = true;
    } else {
      let count: any;
      const _d = new Date();
      const exp = _d.setFullYear(_d.getFullYear() + 1);
      const d_final = new Date(exp);
      if (!this.cookie.get('applangCounter')) {
        this.cookie.put('applangCounter', '0', {
          path: '/',
          expires: d_final
        });
      } else {

        count = this.cookie.get('applangCounter');
        this.cookie.put('applangCounter', (+count + 1).toString(), {
          path: '/',
          expires: d_final
        });
      }
      this.router.events.subscribe((event: any) => {
        if (event instanceof NavigationEnd) {
          const routerLang: any = event.url.split('/')[1];
          this.lang = routerLang === 'ru' ? false : true;

          if (count >= 4) {
            this.cookie.put('applang', routerLang === 'ru' ? 'ru' : 'uk', {
              path: '/',
              expires: d_final
            });
            this.checkLangCookie = true;
          }
        }
      });
    }

    this.subscriptions.add(
      this.service.getMenu().subscribe((resp: any) => {
        if (resp) {
          this.tags = resp.top;
        }
      })
    );
  }

}
