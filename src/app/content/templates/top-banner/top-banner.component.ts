import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {MenuService} from '../menu/menu.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-top-banner',
  templateUrl: './top-banner.component.html',
  styleUrls: ['./top-banner.component.scss']
})
export class TopBannerComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  banner: any = {};
  checkImg = false;
  hidden = false;

  @ViewChild('img', { static: false }) img: ElementRef;

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  check(img) {
    // console.log('img');
    if (img.naturalHeight > img.naturalWidth) {
      this.checkImg = true;
    } else {
      this.checkImg = false;
    }
  }

  constructor(private service: MenuService, public router: Router, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.router.events.subscribe((ev: any) => {
      if (ev instanceof NavigationEnd) {
        // console.log(this.router.url.search('/search/tag/'));
        // console.log(this.router.url.search('/tag/'));
        if (this.router.url.includes('/tag/') && !this.router.url.includes('/search/tag/')) {
          this.hidden = true;
        } else {
          this.hidden = false;
        }
        // console.log(this.hidden)
      }
    });
    this.subscriptions.add(
      this.service.getMenu().subscribe((resp: any) => {
        if (resp) {
          this.banner = resp.banner;
        }
      })
    );
  }

}
