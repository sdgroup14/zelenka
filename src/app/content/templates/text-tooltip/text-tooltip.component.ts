import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-text-tooltip',
  templateUrl: './text-tooltip.component.html',
  styleUrls: ['./text-tooltip.component.scss']
})
export class TextTooltipComponent implements OnInit {
  pos: any;
  txt: string;
  @Output() close = new EventEmitter();

  @Input() set text(val) {
    // console.log(val)
    this.txt = val;
  }

  @Input() set position(val) {
    this.pos = val;
    // console.log('val', val);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
