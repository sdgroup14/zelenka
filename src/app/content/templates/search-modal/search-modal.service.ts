import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchModalService {
  public result = new Subject();

  constructor(private http: HttpClient) {
  }

  get(val) {
    return this.http.get(environment.apiUrl + '/search/articles/' + val, {}).subscribe(resp => {
      this.result.next(resp);
    });
  }
}
