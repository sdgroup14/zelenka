import {Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild} from '@angular/core';
import {SearchModalService} from './search-modal.service';
import {Router} from '@angular/router';
import {CommonService} from '../../../services/common.service';
import {DateService} from '../../../services/date.service';

@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.component.html',
  styleUrls: ['./search-modal.component.scss']
})
export class SearchModalComponent implements OnInit {
  @Output() hide = new EventEmitter();
  @ViewChild('inpEl', { static: true }) inpEl: ElementRef;
  txt = '';
  encodeTxt = '';
  articles = [];
  redirectLink = '';

  @Input('isOpen') set isOpen(val) {
    if (val) {
      setTimeout(() => {
        this.inpEl.nativeElement.focus();
      }, 200);
    }
  }

  search() {
    this.encodeTxt = encodeURI(this.txt);
    this.service.get(this.txt);
    // if (this.txt && this.txt.length > 2) {
    //   this.service.get(this.txt);
    //   console.log('123')
    // } else {
    //   this.articles = [];
    // }
  }

  close() {
    this._renderer.removeClass(document.body, 'scrollOff');
    this.hide.emit();
    this.txt = '';
    this.encodeTxt = '';
    this.articles = [];
  }

  getArticleUrl(article, event) {
    event.preventDefault();
    this.close();
    if (article.is_link) {
      window.location = article.redirect_url;
    } else {
      this.router.navigate([this._common.langPath() + '/article', article.slug]);
    }
  }


  constructor(private service: SearchModalService, private _renderer: Renderer2, private router: Router, private _common: CommonService, public date_s: DateService) {
  }

  ngOnInit() {
    this.redirectLink = this._common.langPath() + '/search/articles/';

    this.service.result.subscribe((resp: any) => {
      if (resp.articles.data.length > 10) {
        resp.articles.data.length = 10;
      }
      this.articles = resp.articles.data;
    });
  }

}
