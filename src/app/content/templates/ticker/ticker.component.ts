import {Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PlatformService} from '../../../services/platform.service';
import {CommonService} from '../../../services/common.service';
import {HomeService} from '../../pages/home-page/services/home.service';
import {AppAuthService} from '../../../services/app-auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.scss']
})
export class TickerComponent implements OnInit, OnDestroy {
  position = 0;
  @ViewChild('listEl', { static: true }) listEl: ElementRef;
  @ViewChild('inpEl', { static: true }) inpEl: ElementRef;

  tickers: any = [];
  props: any = {

  };
  fakewidth = 0;
  tick2: any;
  tick3: any;
  animate = false;
  posValue = 20;
  isEdit = false;
  message = '';

  @Input() set setTickers(data) {
    // console.log('123', val);
    this.tickers = data.tickers;
    this.props = data.props;
    // this.tickers = [{text: 'ЩАСЛИВИЙ ШЛЮБ - ЦЕ МИРНЕ СПІВІСНУВАННЯ ДВОХ НЕРВОВИХ СИСТЕМ'}];
    if (this.platform.check()) {
      this.animate = true;
      this.onResize();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    if (this.tickers && this.tickers.length && this.platform.check()) {
      clearInterval(this.tick2);
      clearInterval(this.tick3);
      this.position = 0;
      this.fakewidth = 0;
      if (!window.matchMedia('(max-width: 959px)').matches) {
        this.tick2 = setInterval(() => {
          this.position += this.posValue;
        }, 300);

        this.tick3 = setInterval(() => {
          const child = this.listEl.nativeElement.children[0];
          const childW = child.offsetWidth;
          if (child.getBoundingClientRect().left + childW < 0) {
            this.tickers.push(this.tickers[0]);
            this.fakewidth += childW;
            this.tickers.splice(0, 1);
          }
        }, 1000);
      }
    }
  }

  changeView() {
    if (!this.auth.currentUserValue) {
      this.translate.get('tickers.error_login').subscribe(txt => {
        this.common.notif.next(txt);
      });
      return;
    }
    this.isEdit = !this.isEdit;
    setTimeout(() => {
      this.isEdit ? this.inpEl.nativeElement.focus() : this.inpEl.nativeElement.blur();
    });

  }

  sendMessage() {
    this.service.saveTicker(this.message).subscribe((resp: any) => {
      // console.log('resp', resp);
      if (resp.success) {
        this.message = '';
        this.isEdit = false;
      } else {

      }
      this.common.notif.next(resp.message);
    }, error => {
      // console.log('error', error);
    });
  }

  constructor(private platform: PlatformService,
              private common: CommonService,
              private translate: TranslateService,
              private auth: AppAuthService,
              private service: HomeService) {

  }

  ngOnInit() {
    if (this.platform.check()) {
      this.animate = true;
      this.onResize();
    }
  }

  ngOnDestroy() {
    // clearTimeout(this.tick1);
    // clearInterval(this.tick2);
    // clearInterval(this.tick3);
  }


}
