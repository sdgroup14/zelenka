import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {CommonService} from '../../../services/common.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class GalleryService {
  public caricature = new BehaviorSubject(null);
  public page = new BehaviorSubject('/caricatures');

  change(data): void {
    this.caricature.next(data);
  }

  get(): Observable<any> {
    return this.caricature.asObservable();
  }

  changeCheck(data): void {
    this.page.next(data);
  }

  getCheck(): Observable<any> {
    return this.page.asObservable();
  }


  constructor(private http: HttpClient, private _common: CommonService, private router: Router) {
  }

  getCaricatures(slug) {
    return this.http.get(environment.apiUrl + '/caricature/' + slug, {}).subscribe((resp: any) => {
      resp.slug = slug;
      this.change(resp);
    }, () => {
      this._common.url.next(this.router.url);
      this.router.navigate(['/404']);
    });
  }
}
