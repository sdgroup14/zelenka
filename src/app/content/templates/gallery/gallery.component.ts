import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import Swiper from 'swiper';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {GalleryService} from './gallery.service';
import {PlatformService} from '../../../services/platform.service';
import {MetaService} from '../../../services/meta.service';
import {environment} from '../../../../environments/environment';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscriptions = new Subscription();
  index: any;
  shareIsOpen = false;
  sW = 0;
  sH = 0;
  timeout: any;
  link = '/caricatures';
  swiper: any;
  @ViewChild('wrapEl', { static: true }) wrapEl: ElementRef;
  caricature: any = {};
  slides = [];
  init = false;
  direction: any = true;
  checkExist = false;
  share: any = {
    fb: '',
    tw: '',
    tm: '',
    vb: ''
  };

  constructor(
    private _renderer: Renderer2,
    private service: GalleryService,
    private router: Router,
    public _common: CommonService,
    private sMeta: MetaService,
    private platform: PlatformService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    if (this.platform.check()) {
      this._renderer.addClass(document.body, 'scrollOff');

      this.subscriptions.add(this.service.getCheck().subscribe(page => {
        this.link = page;
      }));
    }
  }

  ngOnDestroy() {
    this.service.change(null);
    if (this.platform.check()) {
      this.subscriptions.unsubscribe();
      this._renderer.removeClass(document.body, 'scrollOff');
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(time?) {
    this.sW = this.sH = this.wrapEl.nativeElement.offsetHeight;
    time = time || 1000;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.swiper.update();
    }, 100);
  }

  changeSlideHandler(slug) {
    const _slide = this.slides.filter(slide => {
      return slide.slug === slug;
    })[0];
    const index = this.slides.indexOf(_slide);
    this.swiper.slideTo(index);
  }


  changeCaricature(next) {
    this.checkExist = false;
    this.direction = next;
    const key = this.direction ? 'next' : 'prev';
    const nextSlug = this.slides[this.swiper.activeIndex][key];
    if (nextSlug) {
      const exist = this.slides.filter(slide => {
        return slide.slug === nextSlug;
      })[0];
      this.router.navigate([this._common.langPath() + '/caricature', nextSlug]);
      if (exist) {
        this.checkExist = true;
        this.share.fb = 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(exist.slug);
        this.share.tw = 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(exist.slug);
        this.share.tm = 'https://telegram.me/share/url?url=' + this.encodeHandler(exist.slug);
        this.share.vb = 'viber://forward?text=' + this.encodeHandler(exist.slug);
        this.caricature.current = exist.current;
        this.caricature.caption = exist.caption;
        this.caricature.total = exist.total;
        this.changeSlideHandler(exist.slug);
      }
    }
  }

  openWindow(link) {
    const w = 560;
    const h = 640;
    const left = (screen.width / 2) - (w / 2);
    const top = (screen.height / 2) - (h / 2);
    return window.open(link, 'window', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }

  encodeHandler(href) {
    return environment.domain + this._common.langPath() + '/caricature/' + encodeURI(href);
  }

  ngAfterViewInit() {
    this.subscriptions.add(this.route.params.subscribe(params => {
      if (params['slug']) {
        if (!this.checkExist) {
          this.service.getCaricatures(params['slug']);
        }
      }
    }));

    this.subscriptions.add(
      this.service.get().subscribe((resp: any) => {
        if (resp) {
          this.sMeta.setMeta(
            resp.current.caption + ' | iod.media',
            resp.current.caption + ' | iod.media',
            '',
            '',
            resp.current.fb_cover ? resp.current.fb_cover : resp.current.img_md,
            this._common.langPath() + '/caricature/' + resp.slug,
            'default'
          );

          this.share.fb = 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(resp.slug);
          this.share.tw = 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(resp.slug);
          this.share.tm = 'https://telegram.me/share/url?url=' + this.encodeHandler(resp.slug);
          this.share.vb = 'viber://forward?text=' + this.encodeHandler(resp.slug);

          if (!this.direction) {
            this.slides.unshift({
              src: resp.current.img_xl,
              slug: resp.slug,
              current: resp.current.order_numb,
              caption: resp.current.caption,
              total: resp.total,
              next: resp.next,
              show: false,
              prev: resp.prev,
              isHide: true
            });
          } else {
            this.slides.push({
              src: resp.current.img_xl,
              slug: resp.slug,
              current: resp.current.order_numb,
              caption: resp.current.caption,
              total: resp.total,
              next: resp.next,
              prev: resp.prev
            });
          }

          if (this.platform.check()) {
            setTimeout(() => {
              this.swiper.update();
            }, 200);
          }
          this.caricature.current = resp.current.order_numb;
          this.caricature.caption = resp.current.caption;
          this.caricature.total = resp.total;
          if (this.platform.check()) {
            setTimeout(() => {
              this.swiper.setIndex = 1;
              this.changeSlideHandler(resp.slug);
              this.onResize(0);
            });
          }
        }
      })
    );

    this.init = true;

    if (this.platform.check()) {
      this.swiper = new Swiper('.swiper-container', {
        direction: 'horizontal',
        autoHeight: true,
        observer: true,
        speed: 1,
        loop: false,
        slidesPerView: 1,
        shortSwipes: false,
        longSwipes: false,
        longSwipesRatio: 25,
        followFinger: false,
        touchEventsTarget: 'wrapper',
        on: {
          touchEnd: () => {
            if (this.swiper.touches.diff > 0) {
              this.changeCaricature(false);
            } else {
              this.changeCaricature(true);
            }
          },

        }
      });
    }
  }


}
