import {Component, OnInit} from '@angular/core';
import {PreloaderService} from './preloader.service';
import {PlatformService} from '../../../services/platform.service';

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.scss']
})
export class PreloaderComponent implements OnInit {
  isOpen = false;

  constructor(private preloader: PreloaderService, private platform: PlatformService) {}

  ngOnInit() {
    this.preloader.get().subscribe(boolean => {
      // console.log('123');
      if (this.platform.check()) {
        this.isOpen = boolean;
      }
    });
  }


}
