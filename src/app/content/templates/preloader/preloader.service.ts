import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreloaderService {
  public visibillity = new BehaviorSubject(true);

  public show(): void {
    this.visibillity.next(false);
  }

  public hide(): void {
    setTimeout(() => {
      this.visibillity.next(true);
    }, 100);
  }

  public get(): Observable<any> {
    return this.visibillity.asObservable();
  }

  constructor() {
  }
}
