import {Component, EventEmitter, OnInit, Output, Renderer2} from '@angular/core';
// import {AngularFireAuth} from '@angular/fire/auth';
// import * as firebase from 'firebase/app';
import {AppAuthService} from '../../../services/app-auth.service';
import {CookiesService} from '@ngx-utils/cookies';
import {CommonService} from '../../../services/common.service';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {
  @Output() hide = new EventEmitter();
  email = '';
  error = '';

  constructor(
    private _renderer: Renderer2,
    private router: Router,
    private cookie: CookiesService,
    private common: CommonService,
    public appAuth: AppAuthService) {
  }

  // signInWithGoogle() {
  //   return new Promise<any>(() => {
  //     const provider = new firebase.auth.GoogleAuthProvider();
  //     this.afAuth.auth.signInWithRedirect(provider);
  //   });
  // }
  //
  // signInWithFB() {
  //   return new Promise<any>(() => {
  //     const provider = new firebase.auth.FacebookAuthProvider();
  //     this.afAuth.auth.signInWithRedirect(provider);
  //   });
  // }
  //
  // signInWithTwitter() {
  //   return new Promise<any>(() => {
  //     const provider = new firebase.auth.TwitterAuthProvider();
  //     this.afAuth.auth.signInWithRedirect(provider);
  //   });
  // }
  signInWithGoogle() {
    this.router.navigate([{outlets: {modals: null}}]);
    // console.log(window.location.pathname)
    localStorage.setItem('appRedirect', window.location.pathname);
    localStorage.setItem('authProvider', 'google');
    // console.log(
    //   localStorage.getItem('appRedirect'))
    window.location.href = 'https://accounts.google.com/o/oauth2/v2/auth?' +
      'client_id=990535061945-o12qj7r8p3183rse30fvkonf7d21uem9.apps.googleusercontent.com&' +
      'response_type=token' +
      '&scope=profile&' +
      'redirect_uri=https://iod.media/auth';

  }

  signInWithFB() {
    this.router.navigate([{outlets: {modals: null}}]);
    localStorage.setItem('appRedirect', window.location.pathname);
    localStorage.setItem('authProvider', 'facebook');
    // alert(localStorage.getItem('appRedirect'))
    window.location.href = 'https://www.facebook.com/v4.0/dialog/oauth?' +
      'client_id=413681322735717&' +
      'response_type=token&' +
      'redirect_uri=https://iod.media/auth&' +
      'auth_type=rerequest&' +
      'scope=email';
  }

  signInWithTwitter() {
    this.router.navigate([{outlets: {modals: null}}]);
    localStorage.setItem('appRedirect', window.location.pathname);
    localStorage.setItem('authProvider', 'twitter');
    this.appAuth.authTwitter().pipe(take(1)).subscribe((resp: any) => {
      window.location.href = 'https://api.twitter.com/oauth/authenticate?oauth_token=' + resp.oauth_token;
    });
  }

  submit() {
    this.appAuth.loginWithEmail(this.email).subscribe((resp: any) => {
      if (!resp.hasOwnProperty('errors')) {
        this.hide.emit(false);
        this.common.notif.next(resp.message);
        this._renderer.removeClass(document.body, 'scrollOff');
        this.email = '';
        setTimeout(() => {
          // console.log('close')
          this.common.notif.next(null);
        }, 4000);
      } else {
        this.error = resp.errors.email;
      }

    });
  }

  ngOnInit() {
    // this.afAuth.auth.getRedirectResult().then((result: any) => {
    //   if (result.credential && result.credential.providerId !== 'twitter.com' && !this.cookie.get('appToken')) {
    //     const provider = result.credential.providerId.split('.')[0];
    //     const token = result.credential.accessToken;
    //     this.appAuth.loginWithSocial(token, provider);
    //   }
    //   if (result.credential && result.credential.providerId === 'twitter.com' && !this.cookie.get('appToken')) {
    //     const provider = result.credential.providerId.split('.')[0];
    //     const token = result.credential.accessToken;
    //     const secret = result.credential.secret;
    //     this.appAuth.loginWithSocial(token, provider, secret);
    //   }
    // }).catch((error: any) => {
    //   if (error.credential && error.credential.providerId === 'twitter.com' && !this.cookie.get('appToken')) {
    //     const provider = error.credential.providerId.split('.')[0];
    //     const token = error.credential.accessToken;
    //     const secret = error.credential.secret;
    //     this.appAuth.loginWithSocial(token, provider, secret);
    //   }
    //   if (error.credential && error.credential.providerId !== 'twitter.com' && !this.cookie.get('appToken')) {
    //     const provider = error.credential.providerId.split('.')[0];
    //     const token = error.credential.accessToken;
    //     this.appAuth.loginWithSocial(token, provider);
    //   }
    // });
  }

}
