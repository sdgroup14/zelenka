import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {SearchService} from '../services/search.service';
import {Subscription} from 'rxjs';
import {DateService} from '../../../../services/date.service';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {PlatformService} from '../../../../services/platform.service';
import {MetaService} from '../../../../services/meta.service';
import {CommonService} from '../../../../services/common.service';
import {TranslateService} from '@ngx-translate/core';
import {Location, ViewportScroller} from '@angular/common';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  data: any = [];
  tagName = '';
  scrollPosition;
  pagination: any = {
    currentPage: 1,
    page: 1,
    lastPage: null,
    route: null,
    slug: null,
  };

  constructor(private service: SearchService,
              public date_s: DateService,
              private preloader: PreloaderService,
              private platform: PlatformService,
              public _common: CommonService,
              private translate: TranslateService,
              private sMeta: MetaService,
              private router: Router,
              private location: Location,
              private route: ActivatedRoute,
              private viewportScroller: ViewportScroller) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  ngOnDestroy() {
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

  changePage() {
    this.service.getApiSearchNext(this.pagination.route, this.pagination.slug, this.pagination.page);
  }

  getArticleUrl(article, event) {
    event.preventDefault();

    if (event.target.classList[0] !== 'tag') {
      if (article.is_link) {
        window.location = article.redirect_url;
      } else {
        this.router.navigate([this._common.langPath() + '/article', article.slug]);
      }
    }
  }

  ngOnInit() {
    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params['route'] && params['slug']) {
          this.pagination = {
            currentPage: 1,
            page: 1,
            lastPage: null,
            route: null,
            slug: null,
          };
          this.pagination.route = params['route'];
          this.pagination.slug = decodeURI(params['slug']);
          const query_page = this.route.snapshot.queryParams.page;

          this.subscriptions.add(
            this.service.getApiSearch(this.pagination.route, this.pagination.slug, 1, query_page).subscribe((resp: any) => {
              if (resp) {
                this.pagination.page = query_page ? query_page : 1;

                if (this.pagination.route === 'tag') {
                  this.translate.get('search.metaTitle').subscribe(text => {
                    this.sMeta.setMeta(
                      resp.tag.seo_title ? resp.tag.seo_title + ' | iod.media' : text + ' ' + resp.tag.name + ' | iod.media',
                      text + ' ' + resp.tag.name + ' | iod.media',
                      resp.tag.seo_descr,
                      resp.tag.seo_keywords,
                      '',
                      this._common.langPath() + '/search/' + params['route'] + '/' + params['slug'],
                      'default'
                    );
                  });
                  this.tagName = resp.tag.name;
                } else {
                  this.translate.get('search.metaTitle-articles').subscribe(text => {
                    this.sMeta.setMeta(
                      text + ' ' + this.pagination.slug + ' | iod.media',
                      text + ' ' + this.pagination.slug + ' | iod.media',
                      '',
                      this.pagination.slug,
                      '',
                      this._common.langPath() + '/search/' + params['route'] + '/' + params['slug'],
                      'default'
                    );
                  });
                }


                this.data = resp.articles.data;
                this.pagination.total = resp.articles.total;
                this.pagination.lastPage = resp.articles.last_page;
                this.pagination.currentPage = resp.articles.current_page;
                if (+this.pagination.page !== +this.pagination.lastPage) {
                  this.pagination.page++;
                }
                this.preloader.hide();
                setTimeout(() => {
                  this.viewportScroller.scrollToPosition(this.scrollPosition);
                }, 10);
              }
            }, err => {
              this._common.url.next(this.router.url);
              this.router.navigate(['/404']);
            })
          );
        }
      })
    );

    this.subscriptions.add(
      this.service.getSearch().subscribe((resp: any) => {
        if (resp) {
          let url = this.router.url;
          if (url.indexOf('?') > 0) {
            url = url.split('?')[0];
          }

          this.location.replaceState(url, 'page=' + this.pagination.page, history.state);
          this.data = this.data.concat(resp.articles.data);

          this.pagination.total = resp.articles.total;
          this.pagination.lastPage = resp.articles.last_page;
          this.pagination.currentPage = resp.articles.current_page;
          if (+this.pagination.page !== +this.pagination.lastPage) {
            this.pagination.page++;
          }
          this.preloader.hide();
        }
      })
    );


  }

}
