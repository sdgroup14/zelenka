import { NgModule } from '@angular/core';

import { SearchPageRoutingModule } from './search-page-routing.module';
import {SearchListComponent} from './search-list/search-list.component';
import {SharedModule} from '../../../modules/shared.module';
import {SearchService} from './services/search.service';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [SearchListComponent],
  imports: [
    SharedModule,
    SearchPageRoutingModule,
    LazyLoadImageModule
  ],
  providers: [SearchService]
})
export class SearchPageModule { }
