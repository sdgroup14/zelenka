import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchListComponent} from './search-list/search-list.component';

const routes: Routes = [
  {
    path: ':route/:slug',
    component: SearchListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchPageRoutingModule { }
