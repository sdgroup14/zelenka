import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public search = new Subject();

  changeSearch(data): void {
    this.search.next(data);
  }

  getSearch(): Observable<any> {
    return this.search.asObservable();
  }

  constructor(private http: HttpClient) {
  }

  getApiSearch(route, slug, page, check) {
    let query = '';
    if (check > 1) query = '&from_zero=' + check;
    return this.http.get(environment.apiUrl + '/search/' + route + '/' + slug + '?page=' + page + query, {});
  }

  getApiSearchNext(route, slug, page) {
    return this.http.get(environment.apiUrl + '/search/' + route + '/' + slug + '?page=' + page, {}).subscribe(resp => {
      this.changeSearch(resp);
    });
  }
}
