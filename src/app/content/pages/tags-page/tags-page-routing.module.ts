import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TagsListComponent} from './tags-list/tags-list.component';
import {TagsListModifyComponent} from "./tags-list-modify/tags-list-modify.component";

const routes: Routes = [
  {
    path: 'nevidomiukraintsi',
    component: TagsListModifyComponent,
    data: {
      topBannerIsHidden: true
    }
  },
  {
    path: ':slug',
    component: TagsListComponent,
    data: {
      topBannerIsHidden: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TagsPageRoutingModule {
}
