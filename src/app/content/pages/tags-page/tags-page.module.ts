import { NgModule } from '@angular/core';

import { TagsPageRoutingModule } from './tags-page-routing.module';
import {TagsListComponent} from './tags-list/tags-list.component';
import {TagsService} from './services/tags.service';
import {SharedModule} from '../../../modules/shared.module';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {TagsListModifyComponent} from "./tags-list-modify/tags-list-modify.component";

@NgModule({
  declarations: [TagsListComponent, TagsListModifyComponent],
  imports: [
    TagsPageRoutingModule,
    SharedModule,
    LazyLoadImageModule
  ],
  providers: [TagsService]
})
export class TagsPageModule { }
