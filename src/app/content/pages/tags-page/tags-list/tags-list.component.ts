import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {TagsService} from '../services/tags.service';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {PlatformService} from '../../../../services/platform.service';
import {DateService} from '../../../../services/date.service';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {MetaService} from '../../../../services/meta.service';
import {CommonService} from '../../../../services/common.service';
import {environment} from '../../../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {filter} from 'rxjs/operators';
import {ViewportScroller, Location} from '@angular/common';

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.scss']
})

export class TagsListComponent implements OnInit, OnDestroy {

  constructor(private service: TagsService,
              private route: ActivatedRoute,
              public date_s: DateService,
              private router: Router,
              private translate: TranslateService,
              public _common: CommonService,
              private sMeta: MetaService,
              private preloader: PreloaderService,
              private platform: PlatformService,
              private location: Location,
              private viewportScroller: ViewportScroller) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));

  }

  private subscriptions = new Subscription();
  @ViewChild('boxEl', { static: true }) boxEl: ElementRef;
  scrollPosition;
  imgIsAbs = false;
  articles: any = [];
  tag: any = {
    cover_fb: '',
    cover_xl: '',
  };

  pagination: any = {
    currentPage: 1,
    page: 1,
    lastPage: null,
    slug: null
  };

  share: any = {
    fb: '',
    tw: '',
    tm: '',
    vb: ''
  };

  pageTitle = '';

  @HostListener('window:scroll', ['$event'])
  onScroll(e?) {
    const outletHeight = this.boxEl.nativeElement.parentNode.parentNode.offsetHeight;
    const outletOffsetTop = this.boxEl.nativeElement.parentNode.parentNode.offsetTop;
    let scrollTop;
    if (e) {
      scrollTop = e.target.scrollingElement.scrollTop;
    } else {
      scrollTop = window.scrollY;
    }
    this.imgIsAbs = (scrollTop + window.innerHeight) >= (outletHeight + outletOffsetTop);
  }

  ngOnDestroy() {
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

  changePage() {
    this.service.getApiArticles(this.pagination.slug, this.pagination.page);
  }

  commonPageHandler(last, current) {
    this.pagination.lastPage = last;
    this.pagination.currentPage = current;
    if (+this.pagination.page !== +this.pagination.lastPage) {
      this.pagination.page++;
    }
  }

  getArticleUrl(article, event) {
    event.preventDefault();
    if (article.is_link) {
      window.location = article.redirect_url;
    } else {
      this.router.navigate([this._common.langPath() + '/article', article.slug]);
    }
  }

  openWindow(link) {
    const w = 560;
    const h = 640;
    const left = (screen.width / 2) - (w / 2);
    const top = (screen.height / 2) - (h / 2);
    return window.open(link, 'window', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }

  encodeHandler(href) {
    return environment.domain + this._common.langPath() + '/tag/' + encodeURI(href);
  }

  ngOnInit() {
    this.subscriptions.add(this.route.params.subscribe((params: any) => {
      if (params['slug']) {
        this.pagination.slug = params['slug'];
        const query_page = this.route.snapshot.queryParams.page;
        this.subscriptions.add(
          this.service.getTag(this.pagination.slug, 1, query_page).subscribe((resp: any) => {
            this.pagination.page = query_page ? query_page : 1;
            this.tag = resp.tag;
            this.articles = resp.articles.data;
            this.share.fb = 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(this.pagination.slug);
            this.share.tw = 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(this.pagination.slug);
            this.share.tm = 'https://telegram.me/share/url?url=' + this.encodeHandler(this.pagination.slug);
            this.share.vb = 'viber://forward?text=' + this.encodeHandler(this.pagination.slug);
            this.translate.get('tagMain.metaTitle').subscribe(text => {
              this.pageTitle = this.tag.seo_title ? this.tag.seo_title + ' | iod.media' : text + ' ' + this.tag.name + ' | iod.media';
              this.sMeta.setMeta(
                this.pageTitle,
                text + ' ' + this.tag.name + ' | iod.media',
                this.tag.seo_descr,
                this.tag.seo_keywords,
                this.tag.cover_fb ? this.tag.cover_fb : this.tag.cover_xl,
                this._common.langPath() + '/tag/' + this.pagination.slug,
                'default'
              );
            });
            this.pagination.total = resp.articles.total;
            this.commonPageHandler(resp.articles.last_page, this.pagination.page);
            setTimeout(() => {
              if (this.platform.check()) {
                this.onScroll();
              }
            }, 1);
            this.preloader.hide();
            setTimeout(() => {
              this.viewportScroller.scrollToPosition(this.scrollPosition);
            }, 10);
          }, () => {
            this._common.url.next(this.router.url);
            this.router.navigate(['/404']);
          })
        );
      }
    }));

    this.subscriptions.add(this.service.getArticles().subscribe((resp: any) => {
      let url = this.router.url;
      if (url.indexOf('?') > 0) {
        url = url.split('?')[0];
      }
      this.location.replaceState(url, 'page=' + this.pagination.page, history.state);
      this.articles = this.articles.concat(resp.articles.data);
      this.commonPageHandler(resp.articles.last_page, resp.articles.current_page);
      setTimeout(() => {
        if (this.platform.check()) {
          this.onScroll();
        }
      }, 1);

    }));

  }

}
