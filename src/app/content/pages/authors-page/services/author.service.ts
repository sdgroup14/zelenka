import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  public articles = new Subject();

  changeArticles(data): void {
    this.articles.next(data);
  }

  getArticles(): Observable<any> {
    return this.articles.asObservable();
  }

  constructor(private http: HttpClient) {
  }

  getAuthor(author, page, check) {
    let query = '';
    if (check > 1) query = '&from_zero=' + check;
    return this.http.get(environment.apiUrl + '/author/' + author + '?page=' + page + query, {});
  }

  getApiArticles(slug, page) {
    return this.http.get(environment.apiUrl + '/author/' + slug + '?page=' + page, {}).subscribe(resp => {
      this.changeArticles(resp);
    });
  }


}
