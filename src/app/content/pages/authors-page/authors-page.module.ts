import {NgModule} from '@angular/core';

import {AuthorsPageRoutingModule} from './authors-page-routing.module';
import {AuthorsListComponent} from './authors-list/authors-list.component';
import {SharedModule} from '../../../modules/shared.module';
import {AuthorService} from './services/author.service';
import {LazyLoadImageModule} from 'ng-lazyload-image';

@NgModule({
  declarations: [AuthorsListComponent],
  imports: [
    AuthorsPageRoutingModule,
    SharedModule,
    LazyLoadImageModule
  ],
  providers: [AuthorService]
})
export class AuthorsPageModule {
}
