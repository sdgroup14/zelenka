import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthorService} from '../services/author.service';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {DateService} from '../../../../services/date.service';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {MetaService} from '../../../../services/meta.service';
import {CommonService} from '../../../../services/common.service';
import {filter} from 'rxjs/operators';
import {Location, ViewportScroller} from '@angular/common';

@Component({
  selector: 'app-authors-list',
  templateUrl: './authors-list.component.html',
  styleUrls: ['./authors-list.component.scss']
})
export class AuthorsListComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild('boxEl', { static: true }) boxEl: ElementRef;
  @ViewChild('authorEl', { static: true }) authorEl: ElementRef;
  scrollPosition;
  author: any = {};
  authorFixed = false;
  authorAbsolute = false;
  articles = [];
  pagination: any = {
    currentPage: 1,
    page: 1,
    lastPage: null,
    slug: null
  };

  @HostListener('window:scroll', ['$event'])
  onScroll(e) {
    const outletHeight = this.boxEl.nativeElement.parentNode.parentNode.offsetHeight;
    const outletOffsetTop = this.boxEl.nativeElement.parentNode.parentNode.offsetTop;
    const authorHeight = this.authorEl.nativeElement.offsetHeight;
    const scrollTop = e.target.scrollingElement.scrollTop;
    if (scrollTop > outletOffsetTop - 10) {
      this.authorFixed = true;
    } else {
      this.authorFixed = false;
    }
    if ((scrollTop + authorHeight + 20) >= (outletHeight + outletOffsetTop - 45)) {
      this.authorAbsolute = true;
    } else {
      this.authorAbsolute = false;
    }
  }

  changePage() {
    this.service.getApiArticles(this.pagination.slug, this.pagination.page);
  }

  commonPageHandler(last, current) {
    this.pagination.lastPage = last;
    this.pagination.currentPage = current;
    if (+this.pagination.page !== +this.pagination.lastPage) {
      this.pagination.page++;
    }
  }

  constructor(private service: AuthorService,
              public date_s: DateService,
              private preloader: PreloaderService,
              private router: Router,
              private sMeta: MetaService,
              public _common: CommonService,
              private location: Location,
              private route: ActivatedRoute,
              private viewportScroller: ViewportScroller) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  getArticleUrl(article, event) {
    event.preventDefault();
    if (event.target.classList[0] !== 'tags_item' && event.target.classList[0] !== 'playBtn') {
      if (article.is_link) {
        window.location = article.redirect_url;
      } else {
        this.router.navigate([this._common.langPath() + '/article', article.slug]);
      }
    }
  }

  ngOnDestroy() {
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.route.params.subscribe(params => {
      if (params['slug']) {
        this.pagination.slug = params['slug'];
        const query_page = this.route.snapshot.queryParams.page;
        this.subscriptions.add(
          this.service.getAuthor(this.pagination.slug, 1, query_page).subscribe((resp: any) => {
            this.pagination.page = query_page ? query_page : 1;
            this.author = resp.author;
            this.sMeta.setMeta(
              this.author.name + ' ' + this.author.sirname + ' | iod.media',
              this.author.name + ' ' + this.author.sirname + ' | iod.media',
              this.author.bio,
              this.author.name + ' ' + this.author.sirname,
              this.author.photo,
              this._common.langPath() + '/author/' + this.pagination.slug,
              'default'
            );
            resp.articles.data.map(article => {
              if (article.tags.length > 2) {
                article.tags.length = 2;
              }
            });
            this.pagination.total = resp.articles.total;
            this.articles = resp.articles.data;
            this.commonPageHandler(resp.articles.last_page, resp.articles.current_page);
            this.preloader.hide();
            setTimeout(() => {
              this.viewportScroller.scrollToPosition(this.scrollPosition);
            }, 10);
          }, err => {
            this._common.url.next(this.router.url);
            this.router.navigate(['/404']);
          })
        );
      } else {

      }
    }));

    this.subscriptions.add(this.service.getArticles().subscribe((resp: any) => {
      let url = this.router.url;
      if (url.indexOf('?') > 0) {
        url = url.split('?')[0];
      }
      this.location.replaceState(url, 'page=' + this.pagination.page, history.state);
      this.articles = this.articles.concat(resp.articles.data);
      this.commonPageHandler(resp.articles.last_page, resp.articles.current_page);
    }));
  }

}
