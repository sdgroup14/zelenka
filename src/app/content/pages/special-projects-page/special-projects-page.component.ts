import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {Subscription} from 'rxjs';
import {DateService} from '../../../services/date.service';
import {PreloaderService} from '../../templates/preloader/preloader.service';
import {PlatformService} from '../../../services/platform.service';
import {CommonService} from '../../../services/common.service';
import {TranslateService} from '@ngx-translate/core';
import {MetaService} from '../../../services/meta.service';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {Location, ViewportScroller} from '@angular/common';
import {filter} from 'rxjs/operators';
import {SpecialProjectsPageService} from './special-projects-page.service';

@Component({
  selector: 'app-special-projects-page',
  templateUrl: './special-projects-page.component.html',
  styleUrls: ['./special-projects-page.component.scss']
})
export class SpecialProjectsPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  data: any = [];
  scrollPosition;
  pagination: any = {
    currentPage: 1,
    page: 1,
    lastPage: null,
    route: null
  };
  checkInit = false;

  // infinitySroll = false;

  ngOnDestroy() {
    if (this.platform.check()) {
      this.renderer.removeClass(document.body, '__bg');
    }
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private renderer: Renderer2,
    private service: SpecialProjectsPageService,
    public date_s: DateService,
    private preloader: PreloaderService,
    private platform: PlatformService,
    public _common: CommonService,
    private translate: TranslateService,
    private sMeta: MetaService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller
  ) {
    if (this.platform.check()) {
      this.renderer.addClass(document.body, '__bg');
    }
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  changePage() {
    if (this.pagination.page >= this.pagination.total) {
      return;
    }
    this.service.getApiNewsNext(this.pagination.route, this.pagination.page);
  }

  getArticleUrl(article, event) {
    event.preventDefault();

    if (event.target.classList[0] !== 'tag') {
      if (article.is_link) {
        window.open(article.redirect_url, '_blank');
        // window.location = article.redirect_url;
      } else {
        this.router.navigate([this._common.langPath() + '/article', article.slug]);
      }
    }
  }

  ngOnInit() {
    this.subscriptions.add(
      this.route.params.subscribe(params => {
        // if (params['route'] && params['slug']) {
        this.pagination = {
          currentPage: 1,
          page: 1,
          lastPage: null,
          route: null,
          // slug: null,
        };
        this.pagination.route = params['route'];
        // this.pagination.slug = decodeURI(params['slug']);
        const query_page = this.route.snapshot.queryParams.page;

        this.subscriptions.add(
          this.service.getApiNews(this.pagination.route, 1, query_page).subscribe((resp: any) => {
            if (resp) {
              this.pagination.page = query_page ? query_page : 1;
              if (!this.checkInit) {
                this.checkInit = true;

                this.translate.get('news.seo').subscribe(data => {
                  this.sMeta.setMeta(
                    data.title + ' | iod.media',
                    data.title + ' | iod.media',
                    data.description,
                    data.keywords,
                    'https://static1.iod.media/storage/seo-covers/share_cover.jpg',
                    this._common.langPath() + '/news',
                    'default'
                  );
                });
              }

              resp.data.map(article => {
                article.color = article.color === '#1C1D1F' ? '#ffffff' : article.color;
              });
              // this.color = resp.color === '#1C1D1F' ? '#ffffff' : resp.color;
              // console.log(resp);
              this.data = resp.data;
              this.pagination.total = resp.total;
              this.pagination.lastPage = resp.last_page;
              this.pagination.currentPage = resp.current_page;
              this.pagination.page++;
              this.preloader.hide();
              setTimeout(() => {
                this.viewportScroller.scrollToPosition(this.scrollPosition);
              }, 10);
            }
          }, err => {
            this._common.url.next(this.router.url);
            this.router.navigate(['/404']);
          })
        );
        // }
      })
    );

    this.subscriptions.add(
      this.service.news.subscribe((resp: any) => {
        if (resp) {
          let url = this.router.url;
          if (url.indexOf('?') > 0) {
            url = url.split('?')[0];
          }

          this.location.replaceState(url, 'page=' + this.pagination.page, history.state);
          this.data = this.data.concat(resp.data);

          this.pagination.total = resp.total;
          this.pagination.lastPage = resp.last_page;
          this.pagination.currentPage = resp.current_page;
          this.pagination.page++;
          // if (+this.pagination.page !== +this.pagination.lastPage) {
          // }
          this.preloader.hide();
        }
      })
    );
  }

}
