import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SpecialProjectsPageComponent} from './special-projects-page.component';

const routes: Routes = [
  {
    path: '',
    component: SpecialProjectsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialProjectsPageRoutingModule { }
