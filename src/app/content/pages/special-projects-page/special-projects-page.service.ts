import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpecialProjectsPageService {

  public news = new Subject();


  constructor(private http: HttpClient) {
  }


  getApiNews(route, page, check) {
    let query = '';
    if (check > 1) query = '&from_zero=' + check;
    return this.http.get(environment.apiUrl + '/special-projects?page=' + page + query, {});
  }

  getApiNewsNext(route, page) {
    return this.http.get(environment.apiUrl + '/special-projects?page=' + page, {}).subscribe(resp => {
      this.news.next(resp);
    });
  }
}
