import { NgModule } from '@angular/core';

import { SpecialProjectsPageRoutingModule } from './special-projects-page-routing.module';
import {SpecialProjectsPageComponent} from './special-projects-page.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {SharedModule} from '../../../modules/shared.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [SpecialProjectsPageComponent],
  imports: [
    SharedModule,
    SpecialProjectsPageRoutingModule,
    InfiniteScrollModule,
    LazyLoadImageModule
  ]
})
export class SpecialProjectsPageModule { }
