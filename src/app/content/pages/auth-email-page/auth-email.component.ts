import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AppAuthService} from '../../../services/app-auth.service';
import {PlatformService} from '../../../services/platform.service';

@Component({
  selector: 'app-auth-email',
  templateUrl: './auth-email.component.html',
  styleUrls: ['./auth-email.component.scss']
})
export class AuthEmailComponent implements OnInit {

  constructor(private route: ActivatedRoute, public platform: PlatformService, private appAuth: AppAuthService) {
  }

  ngOnInit() {
    if (this.platform.check()) {
      this.route.queryParams.subscribe((data: any) => {
        if (Object.keys(data).length) {
          for (let opt in data) {
            if (opt === 'token') {
              this.appAuth.loginWithSocial(data[opt], 'email');
            }
          }
        }
      });
    }

  }

}
