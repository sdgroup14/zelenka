import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ArticlePageRoutingModule} from './article-page-routing.module';
import {ArticleRootComponent} from './article-root/article-root.component';
import {ArticleService} from './article-root/services/article.service';
import {SafeHtmlPipe} from './safe-html.pipe';
import {SharedModule} from '../../../modules/shared.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {FormsModule} from '@angular/forms';
import {LoginModalSmallComponent} from '../../templates/login-modal-small/login-modal-small.component';
import {TipHeightPipe} from './tip-height.pipe';
import {TextTooltipComponent} from '../../templates/text-tooltip/text-tooltip.component';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {SliderFullScreenComponent} from "../../templates/slider-full-screen/slider-full-screen.component";
import {ImgVhPipe} from "../../../pipes/img-vh.pipe";

@NgModule({
  declarations: [
    ArticleRootComponent,
    SafeHtmlPipe,
    TipHeightPipe,
    LoginModalSmallComponent,
    TextTooltipComponent,
    SliderFullScreenComponent,
    ImgVhPipe
  ],
  imports: [
    CommonModule,
    ArticlePageRoutingModule,
    SharedModule,
    InfiniteScrollModule,
    FormsModule,
    LazyLoadImageModule
  ],
  providers: [ArticleService]
})
export class ArticlePageModule {
}
