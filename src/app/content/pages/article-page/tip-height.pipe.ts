import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'tipHeight'
})
export class TipHeightPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const imgVal = (value.tip.clientWidth * value.img.naturalHeight) / value.img.naturalWidth;
    const tipHeight = value.tip.clientHeight;
    return imgVal > tipHeight ? imgVal : tipHeight;
  }

}
