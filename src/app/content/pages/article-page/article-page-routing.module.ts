import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ArticleRootComponent} from './article-root/article-root.component';

const routes: Routes = [
  {
    path: ':slug',
    component: ArticleRootComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlePageRoutingModule {
}
