import {Component, ElementRef, HostListener, Inject, NgZone, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {ArticleService} from './services/article.service';
import {DateService} from '../../../../services/date.service';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import Swiper from 'swiper';
import {PlatformService} from '../../../../services/platform.service';
import {DOCUMENT, Location, ViewportScroller} from '@angular/common';
import {MetaService} from '../../../../services/meta.service';
import {CommonService} from '../../../../services/common.service';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';
import {AppAuthService} from '../../../../services/app-auth.service';
import {TranslateService} from '@ngx-translate/core';
import {filter} from 'rxjs/operators';
import {IAppState} from "../../../../store/state/app.state";
import {select, Store} from "@ngrx/store";
import {selectFullScreenSliderIsOpen} from "../../../../store/selectors/article.selector";
import {showSliderModal} from "../../../../store/actions/article.actions";

declare var twttr: any;
declare var instgrm: any;

@Component({
  selector: 'app-article-root',
  templateUrl: './article-root.component.html',
  styleUrls: ['./article-root.component.scss']
})
export class ArticleRootComponent implements OnInit, OnDestroy {
  fullScreenSliderIsOpen$ = this.store.pipe(select(selectFullScreenSliderIsOpen))

  constructor(private route: ActivatedRoute,
              private service: ArticleService,
              private store: Store<IAppState>,
              private rerender: Renderer2,
              private zone: NgZone,
              private sanitizer: DomSanitizer,
              private preloader: PreloaderService,
              private platform: PlatformService,
              private appAuth: AppAuthService,
              private location: Location,
              private sMeta: MetaService,
              private translate: TranslateService,
              public _common: CommonService,
              public router: Router,
              public date_s: DateService,
              private viewportScroller: ViewportScroller,
              @Inject(DOCUMENT) private doc
  ) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      this.checkPopState = true;
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  infinityScrollCheck = true;
  scrollPosition;
  scrollYPosition = 0;
  articlesHeight = 0;
  checkPopState = false;
  domain;
  tooltipPos = {
    x: 0,
    y: 0
  };
  tooltipTxt = '';
  ioSlug = '';

  private subscriptions = new Subscription();
  @ViewChild('shareEl', {static: false}) shareEl: ElementRef;
  @ViewChild('fixedEl', {static: false}) fixedEl: ElementRef;
  @ViewChild('list', {static: true}) listEl: ElementRef;
  articles: any = [];
  socialIsFixed = false;
  data = [];
  top: any = {
    cover_type: 3,
    main_tag: {},
    author: {}
  };
  checkPopstateHostlistener = false;
  pageInit = false;
  lastScrollTop = 0;
  lastArticle = 0;

  shareHidden = false;
  checkEl: any;
  articleIndexShare = 0;
  share: any = {
    fb: '',
    tw: '',
    tm: '',
    vb: ''
  };
  imgWrap = false;
  currentImg: any = null;

  comment: any = {
    show: false,
    text: '',
    colors: [
      {
        color: '#28302C',
        active: true
      },
      {
        color: '#F5D000',
        active: false
      },
      {
        color: '#00D474',
        active: false
      },
      {
        color: '#FF233C',
        active: false
      },
      {
        color: '#08C1FF',
        active: false
      },
      {
        color: '#ffffff',
        active: false
      }
    ]
  };
  @ViewChild('paintCanvas', {static: false}) paintCanvas: ElementRef;
  @ViewChild('paintCanvasBox', {static: false}) paintCanvasBox: ElementRef;
  canvas: any;
  ctx: any;

  pos = {x: 0, y: 0};

  auth = null;

  nextSlugs: any;

  changePaintColor(btn, block) {
    btn.active = true;
    block.colors.map(_btn => {
      if (_btn.color !== btn.color) {
        _btn.active = false;
      }
    });
  }

  removeBlur(img) {
    this.rerender.addClass(img, '__hide');
  }
  imgVh(img){
    if(this.platform.check()){
      const padding = 5 * window.innerHeight / 100;
      if(img.offsetWidth > window.innerWidth){
        this.rerender.setStyle(img, 'width', '100%')
        if(img.offsetHeight > window.innerHeight){
          this.rerender.setStyle(img, 'height', (window.innerHeight - padding) + 'px')
          this.rerender.setStyle(img, 'width', 'auto')
        }

      } else {
        this.rerender.setStyle(img, 'height', (window.innerHeight - padding) + 'px')
        // this.rerender.setStyle(img.parentElement, '__width')
      }
    }

  }

  nextTestHandler(test, i, block, el, qEl) {
    if (test.checked && block.questions.length !== (i + 1)) {
      test.current = false;
      block.questions[i + 1].current = true;

      setTimeout(() => {
        this.scroll(el);
      }, 1);

    } else if (test.checked && block.questions.length === (i + 1)) {
      const api_data = {
        test_id: block.test.id,
        answers: [],
      };
      block.questions.map(q => {
        const answer = q.answers.filter(anw => {
          return anw.checked_api;
        })[0];
        const a_i = q.answers.indexOf(answer);
        api_data.answers.push({
          q_id: q.id,
          ans_id: answer.id,
        });
      });

      this.service.saveTest(api_data.test_id, api_data.answers).subscribe((resp: any) => {
        block.finish = true;
        block.test.result = resp.result;
        block.test.cover_query_param = resp.cover_query_param;
      });
    }

  }

  scroll(el) {
    el.scrollIntoView({behavior: 'smooth', block: 'start'});
  }

  ckeckTestAnswer(q, answ, i, arr, qEl) {
    setTimeout(() => {
      if (!q.checked) {
        q.checked = true;
        if (arr.length !== (i + 1)) {
          q.img = null;
          q.video = null;
        }
        answ.checked = true;
        answ.checked_api = true;
        if (!answ.is_true) {
          q.answers
            .filter(_a => {
              return _a.is_true;
            })
            .map(__a => {
              __a.checked = true;
            });


        }
        if (answ.descr) {
          q.img = answ.cover;
          q.video = answ.video;
          q.descr = answ.descr;
          const video: HTMLVideoElement = qEl.getElementsByTagName('video')[0];
          if (video) {
            video.muted = true;
            video.load();
          }
        }
      }
    });
  }


  pollAnimation(answers) {
    answers.map((item: any) => {
      const intervalTime = 600 / item.percent;
      item.timer = setInterval(() => {
        if (item.startPercent < item.percent) {
          item.startPercent++;
        } else {
          clearInterval(item.timer);
        }
      }, intervalTime);
    });
  }

  opinionsCheck(data, answ) {

    this.service.savePoll(data.resp.poll.id, answ).subscribe((resp: any) => {
      if (resp.success) {
        resp.answers.map(ans => {
          ans.startPercent = 0;
        });
        data.resp = resp;
        setTimeout(() => {
          data.resp.opinionsChoice = true;
          this.pollAnimation(data.resp.answers);
        }, 1);
      } else {
        this._common.notif.next(resp.message);
      }
    });

  }


  showFullScreenSlider(item){
    this.store.dispatch(showSliderModal({value: true, slides: item.slides, caption: item.caption}))
  }

  openWindow(link, param?) {
    const w = 560;
    const h = 640;
    const left = (screen.width / 2) - (w / 2);
    const top = (screen.height / 2) - (h / 2);
    let _l = link;
    if (param) {
      _l = _l + encodeURIComponent('?' + param);
    }
    return window.open(_l, 'window', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }

  sharePositionCommon(scrollVal) {
    const elements = [];
    [].push.apply(elements, document.getElementsByClassName('top_cover'));
    [].push.apply(elements, document.getElementsByClassName('similar'));
    [].push.apply(elements, document.getElementsByClassName('img_full'));
    [].push.apply(elements, document.getElementsByClassName('img_full_vh'));
    [].push.apply(elements, document.querySelectorAll('h1'));
    for (let i = 0, l = elements.length; i < l; i++) {
      const el = elements[i];
      if (el.getBoundingClientRect().left <= this.fixedEl.nativeElement.getBoundingClientRect().left && this.socialIsFixed) {
        const elTop = el.getBoundingClientRect().top + scrollVal;
        if (!this.shareHidden) {
          if (elTop <= (scrollVal + this.fixedEl.nativeElement.offsetHeight + 70) && scrollVal <= (elTop + el.offsetHeight)) {
            this.shareHidden = true;
            this.checkEl = el;
          }
        } else {
          const checkTop = this.checkEl.getBoundingClientRect().top + scrollVal;
          if (scrollVal > (checkTop + this.checkEl.offsetHeight) || checkTop > (scrollVal + this.fixedEl.nativeElement.offsetHeight + 70)) {
            this.checkEl = null;
            this.shareHidden = false;
          }
        }
      }
    }

    const articles = document.querySelectorAll('.article');
    for (let i = 0, l = articles.length; i < l; i++) {
      const el: any = articles[i];
      const elTop = el.getBoundingClientRect().top + scrollVal;
      if (elTop <= (scrollVal + this.fixedEl.nativeElement.offsetHeight + 70) && scrollVal <= (elTop + el.offsetHeight)) {
        // const count = el.innerHTML.split(' ').length;
        if (this.articleIndexShare !== i) {
          this.articleIndexShare = i;
          this.share.fb = 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(this.articles[this.articleIndexShare].slug);
          this.share.tw = 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(this.articles[this.articleIndexShare].slug);
          this.share.tm = 'https://telegram.me/share/url?url=' + this.encodeHandler(this.articles[this.articleIndexShare].slug);
          this.share.vb = 'viber://forward?text=' + this.encodeHandler(this.articles[this.articleIndexShare].slug);
        }
      }
    }
  }

  changeUrlShare(i) {
    this.lastArticle = i;
    const articles = document.querySelectorAll('.article');
    const get_text = (_el) => {
      let ret = '';
      const length = _el.childNodes.length;
      for (let _i = 0; _i < length; _i++) {
        const node = _el.childNodes[_i];
        if (node.nodeType !== 8) {
          ret += node.nodeType !== 1 ? node.nodeValue : get_text(node);
        }
      }
      return ret;
    };
    const words = get_text(articles[this.lastArticle]);
    const count = words.split(' ').length;
    // console.log(articles[this.lastArticle]);
    const param = this.route.snapshot.queryParamMap.get('test_result');
    const query = param ? '?test_result=' + param : '';
    this.ioSlug = this.domain + '/article' + this.articles[this.lastArticle].slug;
    this.sMeta.changeIoArticle(
      this._common.langPath() + '/article/' + this.articles[this.lastArticle].slug + query,
      this.articles[this.lastArticle].top.seo_title ? this.articles[this.lastArticle].top.seo_title + ' | iod.media' : this.articles[this.lastArticle].top.title + ' | iod.media',
      'article',
      this.articles[this.lastArticle].top.author.name,
      this.articles[this.lastArticle].top.main_tag.name,
      this.articles[this.lastArticle].top.publish_time,
      count);
  }


  closeImgWrap() {
    this.imgWrap = false;
    setTimeout(() => {
      this.currentImg = null;
    }, 150);
  }


  @HostListener('window:popstate', ['$event']) prevBtnBrowserHandler(e) {
    setTimeout(() => {
      if (this.router.url.split('/')[1] === 'article') {
        this.checkPopstateHostlistener = true;
        this.articles = [];
        let url: any = this.router.url.split('/');
        url = url[url.length - 1];
        this.service.getArticle(url, true);
      }
    }, 1);
  }

  @HostListener('document:scroll', ['$event']) onScroll(e: any = {}) {
    this.tooltipTxt = '';
    this.closeImgWrap();
    if (this.checkPopState) {
      setTimeout(() => {
        this.checkPopState = false;
      }, 500);
    }
    if (this.shareEl) {
      if (0 >= (this.shareEl.nativeElement.getBoundingClientRect().top - 20)) {
        this.socialIsFixed = true;
      } else {
        this.socialIsFixed = false;
      }
      this.scrollYPosition = window.scrollY;
      const scrollStart = (e.target || document).scrollingElement.scrollTop;
      const scrollEnd = scrollStart + (e.target || document).scrollingElement.offsetHeight;
      this.sharePositionCommon(scrollStart);

      if (scrollStart > this.lastScrollTop) {
        for (let i = 0, l = this.articles.length; i < l; i++) {
          const childrenTop = this.listEl.nativeElement.children[i].offsetTop;
          if (childrenTop < scrollStart || childrenTop > scrollEnd) {
            continue;
          }

          if (this.lastArticle !== i) {
            this.changeUrlShare(i);
          }
        }
      } else {
        for (let i = 0, l = this.articles.length; i < l; i++) {
          const childrenTop = this.listEl.nativeElement.children[i].offsetTop;
          if (childrenTop + this.listEl.nativeElement.children[i].offsetHeight < scrollStart || childrenTop + this.listEl.nativeElement.children[i].offsetHeight > scrollEnd) {
            continue;
          }

          if (this.lastArticle !== i) {
            this.changeUrlShare(i);
          }
        }
      }
      this.lastScrollTop = scrollStart;
    }
  }


  scrollArticles() {
    this.infinityScrollCheck = true;
    if (this.articles.length < 3) {
      const next_slug = this.articles[this.articles.length - 1].nextSlug;
      const check_exists = this.articles.filter(article => {
        return article.slug === next_slug;
      });
      if (!check_exists.length) {
        this.service.getArticle(next_slug, false);
      }
    }
  }

  getArticleUrl(article, event) {
    event.preventDefault();
    this.articles = [];
    this.pageInit = false;
    this.socialIsFixed = false;
    if (event.target.classList[0] !== 'tags_item' && event.target.classList[0] !== 'playBtn') {
      if (article.is_link) {
        window.location = article.redirect_url;
      } else {
        this.router.navigate([this._common.langPath() + '/article', article.slug]);
        this.shareHidden = false;
      }
    }
  }

  ngOnDestroy() {
    if (this.lastArticle > 0) {
      const _history = JSON.parse(sessionStorage.getItem('history')) || [];
      _history.push({
        navigationId: history.state.navigationId,
        position: this.scrollYPosition - this.articlesHeight
      });
      sessionStorage.setItem('history', JSON.stringify(_history));
    }
    this.preloader.show();
    this.service.change(null);
    this.subscriptions.unsubscribe();

  }


  encodeHandler(href) {
    return environment.domain + this._common.langPath() + '/article/' + encodeURI(href);
  }


  savePlayer(player, top) {
    top.player = player;
  }

  topHtml5video(e, articleTop) {
    articleTop.video = e.target.nextSibling;
    articleTop.video.play();
    articleTop.isVideo = true;
    articleTop.video.controls = true;
  }

  html5video(e, item) {
    const video = e.target.offsetParent.nextSibling;
    video.play();
    item.isVideo = true;
    video.controls = true;
  }

  resize(block, el) {
    // console.log(block)
    setTimeout(() => {
      block.ctx.canvas.width = el.offsetWidth;
      block.ctx.canvas.height = el.offsetHeight;
    }, 100);

  }

  setPosition(e, canvas?) {
    if (e.type === 'touchmove') {
      if (canvas) {
        const rect = canvas.getBoundingClientRect();
        this.pos.x = e.touches[0].clientX - rect.left;
        this.pos.y = e.touches[0].clientY - rect.top;
      }

    } else {
      this.pos.x = e.offsetX;
      this.pos.y = e.offsetY;
    }
  }

  draw(e, block, canvas?) {
    this.zone.run(() => {
      if (e.buttons !== 1 && e.type !== 'touchmove') {
        return;
      }
      e.stopPropagation();
      e.preventDefault();
      block.isPicture = true;
      const color = block.colors.filter(col => {
        return col.active;
      })[0].color;
      // console.log(color);
      block.ctx.beginPath();
      block.ctx.lineWidth = 6;
      block.ctx.lineCap = 'round';
      block.ctx.strokeStyle = color;
      block.ctx.moveTo(this.pos.x, this.pos.y);
      if (e.type === 'touchmove') {
        this.setPosition(e, canvas);
      } else {
        this.setPosition(e);
      }

      block.ctx.lineTo(this.pos.x, this.pos.y);
      block.ctx.stroke();
    });

  }

  closePainter(block, el) {
    block.show = false;
    block.isPicture = false;
    //
    if (!block.show && block.ctx) {
      block.ctx.clearRect(0, 0, el.offsetWidth, el.offsetHeight);
    }
  }

  showCanvas(canvas, block, el) {
    if (this.auth) {
      block.show = !block.show;
      //
      if (!block.show && block.ctx) {
        block.ctx.clearRect(0, 0, el.offsetWidth, el.offsetHeight);
      }
      setTimeout(() => {
        if (block.show) {
          block.ctx = canvas.getContext('2d');
          this.resize(block, el);
        }
      }, 100);
    }
  }

  checkAuth(article, i) {
    article.isAuth = true;
    if (!this.auth) {
      this._common.login.next(true);
      setTimeout(() => {
        document.getElementsByClassName('loginModal')[i].scrollIntoView({behavior: 'smooth', block: 'start'});
      }, 100);
    }
  }

  dataURItoBlob(dataURI) {
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type: mimeString});
  }

  showComments(article) {
    if (article.top.comments && !article.commentsInit) {
      this.service.getComments(article.id).subscribe(_resp => {
        article.commentsInit = true;
        article.comments = _resp;
        article.comments.isPicture = false;
        article.comments.show = false;
        article.comments.colors = [
          {
            color: '#28302C',
            active: true
          },
          {
            color: '#F5D000',
            active: false
          },
          {
            color: '#00D474',
            active: false
          },
          {
            color: '#FF233C',
            active: false
          },
          {
            color: '#08C1FF',
            active: false
          },
          {
            color: '#ffffff',
            active: false
          }
        ];
      });
    }
  }

  commentSubmit(article, canvas, list) {
    const data: any = {
      article_id: article.id
    };
    if (article.comments.isPicture) {
      const img_b64 = canvas.toDataURL('image/jpg');
      const blob = this.dataURItoBlob(img_b64);
      data.img = blob;
    }
    if (article.comments.txt) {
      data.text = article.comments.txt;
    }

    this.service.saveComment(data).subscribe((resp: any) => {
      if (resp.success) {
        article.comments.txt = '';
        article.comments.isPicture = false;
        article.comments.show = false;
        if (article.comments.data) {
          article.comments.data.unshift(resp.comment);
        }
        setTimeout(() => {
          list.scrollTop = 0;
        }, 100);

      } else {
        this._common.notif.next(resp.message);
      }
      this.showComments(article);

    });
  }

  messageToUser(name, list: any, inpModel) {
    inpModel.txt = name + ', ';
    setTimeout(() => {
      list.parentNode.scrollIntoView({behavior: 'smooth', block: 'start'});
      setTimeout(() => {
        list.parentNode.lastElementChild.getElementsByTagName('input')[0].focus();
      }, 300);

    }, 1);
  }

  ngOnInit() {
    this.domain = environment.apiUrl;
    this.auth = this.appAuth.currentUserValue;

    this.subscriptions.add(this.appAuth.currentUserSubject.subscribe(data => {
      this.zone.runOutsideAngular(() => {
        this.auth = data;
      });
    }));

    this.subscriptions.add(this.route.params.subscribe(params => {
      if (params['slug']) {
        if (!this.pageInit) {
          this.service.getArticle(params['slug'], true);
        }
      }
    }));

    this.subscriptions.add(
      this.service.get().subscribe((resp: any) => {
        // this.infinityScrollCheck = true;
        if (this.pageInit) {
          this.infinityScrollCheck = false;
        }
        this.pageInit = true;
        if (resp) {
          const top: any = {};
          top.title = resp.title;
          top.seo_title = resp.seo_title;
          top.cover_type = resp.cover_type;
          top.cover_video = resp.cover_video;
          top.cover_embed = resp.cover_embed;
          top.lead = resp.lead;
          top.cover_title = resp.cover_title;
          top.cover_xl = resp.cover_xl;
          top.cover_preload = resp.cover_preload;
          top.publish_time = resp.publish_time;
          top.main_tag = {
            name: resp.tags[0].name,
            slug: resp.tags[0].slug
          };
          top.tags = resp.tags;
          top.comments = resp.comments;
          top.color = resp.color;
          top.descr = resp.descr;
          top.keywords = resp.keywords;
          top.fb_cover = resp.fb_cover;
          top.cover_md = resp.cover_md;
          top.cover_md = resp.cover_md;
          top.chronicle = resp.chronicle;
          top.result_share_cover = resp.result_share_cover;
          if (top.chronicle) {
            top.reverse = true;
          }
          top.author = {
            avatar: resp.author.avatar,
            name: resp.author.name + ' ' + resp.author.sirname,
            slug: resp.author.slug
          };

          if (!this.articles.length) {
            this.share.fb = 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(resp.slug);
            this.share.tw = 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(resp.slug);
            this.share.tm = 'https://telegram.me/share/url?url=' + this.encodeHandler(resp.slug);
            this.share.vb = 'viber://forward?text=' + this.encodeHandler(resp.slug);
          }

          if (top.color === '#1C1D1F') {
            top.color_type = 1;
          } else if (top.color === '#0FD900') {
            top.color_type = 2;
          } else if (top.color === '#00C1FC') {
            top.color_type = 3;
          } else if (top.color === '#752CF5') {
            top.color_type = 4;
          } else if (top.color === '#F7002A') {
            top.color_type = 5;
          } else if (top.color === '#FC4700') {
            top.color_type = 6;
          }
          if (this.platform.check()) {
            setTimeout(() => {
              try {
                (window['adsbygoogle'] = window['adsbygoogle'] || []).push({});
              } catch (e) {
                console.error(e);
              }
            }, 100);
          }

          this.articles.push({
            top: top,
            blocks: resp.blocks,
            id: resp.id,
            slug: resp.slug,
            share: {
              fb: 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + this.encodeHandler(resp.slug),
              tw: 'https://twitter.com/intent/tweet?url=' + this.encodeHandler(resp.slug),
              tm: 'https://telegram.me/share/url?url=' + this.encodeHandler(resp.slug),
              vb: 'viber://forward?text=' + this.encodeHandler(resp.slug)
            },
            comments: {
              colors: [
                {
                  color: '#28302C',
                  active: true
                },
                {
                  color: '#F5D000',
                  active: false
                },
                {
                  color: '#00D474',
                  active: false
                },
                {
                  color: '#FF233C',
                  active: false
                },
                {
                  color: '#08C1FF',
                  active: false
                },
                {
                  color: '#ffffff',
                  active: false
                }
              ]
            }
          });
          // const block = {
          //   order_number: 0,
          //   type: 27,
          //   is_color: null,
          //   content: {
          //     html: `<div class='tableauPlaceholder' id='viz1584812515996' style='position: relative'><noscript><a href='#'><img alt=' ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;mo&#47;monitor_15841091301660&#47;sheet0&#47;1_rss.png' style='border: none'/></a></noscript><object class='tableauViz' style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F'/><param name='embed_code_version' value='3'/><param name='site_root' value=''/><param name='name' value='monitor_15841091301660&#47;sheet0'/><param name='tabs' value='no'/><param name='toolbar' value='yes'/><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;mo&#47;monitor_15841091301660&#47;sheet0&#47;1.png'/><param name='animate_transition' value='yes'/><param name='display_static_image' value='yes'/><param name='display_spinner' value='yes'/><param name='display_overlay' value='yes'/><param name='display_count' value='yes'/></object></div>`
          //   }
          // }
          // this.articles[0].blocks.push(block)
          const param = this.route.snapshot.queryParamMap.get('test_result');
          const query = param ? '?test_result=' + param : '';
          this.sMeta.setMeta(
            this.articles[0].top.seo_title ? this.articles[0].top.seo_title + ' | iod.media' : this.articles[0].top.title + ' | iod.media',
            this.articles[0].top.title + ' | iod.media',
            this.articles[0].top.descr,
            this.articles[0].top.keywords,
            top.result_share_cover ? top.result_share_cover : (this.articles[0].top.fb_cover ? this.articles[0].top.fb_cover : this.articles[0].top.cover_md),
            this._common.langPath() + '/article/' + this.articles[0].slug + query,
            'article',
            {
              publish_time: top.publish_time,
              tag: top.main_tag.name,
              author: top.author.name
            }
          );

          if (this.platform.check()) {

            this.articles.map((article, artI) => {
              article.blocks.map((block, i) => {
                if (block.type === 14) {
                  setTimeout(() => {
                    const sw = new Swiper('.swiper-container-' + artI + i, {
                      direction: 'horizontal',
                      loop: false,
                      slidesPerView: 1,
                      preloadImages: false,
                      observer: true,
                      lazy: {
                        loadPrevNext: true,
                        loadPrevNextAmount: 2,
                      },
                      navigation: {
                        nextEl: '.next-' + artI + i,
                        prevEl: '.prev-' + artI + i
                      },
                    });
                  });
                } else if (block.type === 21) {
                  setTimeout(() => {
                    twttr.widgets.load();
                  });
                } else if (block.type === 23) {
                  setTimeout(() => {
                    instgrm.Embeds.process();
                  });
                } else if (block.type === 25 && !block.hasOwnProperty('resp')) {

                  this.subscriptions.add(this.service.getPoll(block.content.poll_id).subscribe((poll_resp: any) => {
                    poll_resp.answers.map(answ => {
                      answ.startPercent = 0;
                    });
                    block.resp = poll_resp;
                    if (!block.resp.exist) {
                      poll_resp.opinionsChoice = false;
                    } else {
                      poll_resp.opinionsChoice = true;
                      this.pollAnimation(block.resp.answers);
                    }

                  }));
                } else if (block.type === 26 && !block.hasOwnProperty('test')) {

                  this.subscriptions.add(this.service.getTest(block.content.test_id).subscribe((test_resp: any) => {
                    block.test = test_resp.test;
                    test_resp.test.current_n = 1;
                    test_resp.questions[0].current = true;
                    block.questions = test_resp.questions;
                  }));
                } else if (block.type === 27) {
                  block.content.html = `<div class='tableauPlaceholder' id='viz1584812515996-${artI}-${i}' style='position: relative'><noscript><a href='#'><img alt=' ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;mo&#47;monitor_15841091301660&#47;sheet0&#47;1_rss.png' style='border: none'/></a></noscript><object class='tableauViz' style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F'/><param name='embed_code_version' value='3'/><param name='site_root' value=''/><param name='name' value='monitor_15841091301660&#47;sheet0'/><param name='tabs' value='no'/><param name='toolbar' value='yes'/><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;mo&#47;monitor_15841091301660&#47;sheet0&#47;1.png'/><param name='animate_transition' value='yes'/><param name='display_static_image' value='yes'/><param name='display_spinner' value='yes'/><param name='display_overlay' value='yes'/><param name='display_count' value='yes'/></object></div>`

                  setTimeout(() => {
                    let script = this.doc.createElement('script');
                    script.type = 'text/javascript';
                    script.id = 'crona-virus-map';
                    script.text = `var divElement = document.getElementById('viz1584812515996-${artI}-${i}');var vizElement = divElement.getElementsByTagName('object')[0];vizElement.style.width = '100%';vizElement.style.height = '1177px';var scriptElement = document.createElement('script');scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';vizElement.parentNode.insertBefore(scriptElement, vizElement);`;
                    this.doc.body.appendChild(script);
                  }, 1)
                }


              });
            });
            setTimeout(() => {
              this.onScroll()
            }, 300)
          }
          const exists: any = [];
          this.articles.map(article => {
            exists.push(article.id);
            if (article.similars && article.similars.length) {
              article.similars.map(similar => {
                exists.push(similar.id);
              });
            }
          });


          // this.zone.runOutsideAngular(() => {
          setTimeout(() => {
            const tooltips: HTMLCollection = document.getElementsByClassName('p-tooltip');
            // console.log('123', tooltips)
            // if (tooltip) {
            //   tooltip.addEventListener('mouseenter', () => {
            //     console.log('123');
            //   });
            // }
            for (let i = 0; i < tooltips.length; i++) {
              const tooltip = tooltips[i];
              tooltips[i].addEventListener('click', () => {
                // console.log('click')
                const tooltip_bounds: any = tooltip.getBoundingClientRect();
                const tooltip_text = tooltip.getAttribute('data-tooltip');
                this.tooltipPos = {
                  x: window.innerWidth - tooltip_bounds.x - 27 - tooltip_bounds.width,
                  y: window.innerHeight - tooltip_bounds.y + 15
                };
                this.tooltipTxt = tooltip_text;
              }, false);
            }
          });

          // });

          this.preloader.hide();
          this.subscriptions.add(this.service.getSimilar(resp.id, resp.tags[0].id, exists.join(',')).subscribe((data: any) => {
            const current_article = this.articles[this.articles.length - 1];
            if (data && data.length === 3) {
              current_article.similars = data;
            }
            current_article.similars.map(similar => {
              exists.push(similar.id);
            });
            // current_article.nextSlug = data.slugsByTag;
            this.showComments(current_article);

            if (this.articles.length === 1) {

              setTimeout(() => {
                const articles = document.querySelectorAll('.article');
                const get_text = (_el) => {
                  let ret = '';
                  const length = _el.childNodes.length;
                  for (let _i = 0; _i < length; _i++) {
                    const node = _el.childNodes[_i];
                    if (node.nodeType !== 8) {
                      ret += node.nodeType !== 1 ? node.nodeValue : get_text(node);
                    }
                  }
                  return ret;
                };
                const words = get_text(articles[this.lastArticle]);
                const count = words.split(' ').length;
                // console.log(articles[this.lastArticle]);
                const param = this.route.snapshot.queryParamMap.get('test_result');
                const query = param ? '?test_result=' + param : '';
                this.ioSlug = this.domain + '/article' + this.articles[this.lastArticle].slug;
                this.sMeta.changeIoArticle(
                  this._common.langPath() + '/article/' + this.articles[this.lastArticle].slug + query,
                  this.articles[this.lastArticle].top.seo_title ? this.articles[this.lastArticle].top.seo_title + ' | iod.media' : this.articles[this.lastArticle].top.title + ' | iod.media',
                  'article',
                  this.articles[this.lastArticle].top.author.name,
                  this.articles[this.lastArticle].top.main_tag.name,
                  this.articles[this.lastArticle].top.publish_time,
                  count);
              }, 100);


              this.ioSlug = this.domain + '/article' + this.articles[this.articleIndexShare].slug;
              this.subscriptions.add(this.service.getNext(resp.id, resp.tags[0].id, exists.join(',')).subscribe((_data: any) => {
                this.infinityScrollCheck = false;
                // console.log(data);
                this.nextSlugs = _data;
                this.articles[0].nextSlug = this.nextSlugs[0].slug;
                // current_article.nextSlug = data.slugsByTag;
                // const current_article = this.articles[this.articles.length - 1];
                // if (data.similar && data.similar.length === 3) {
                //   current_article.similars = data.similar;
                // }
                // current_article.nextSlug = data.slugsByTag;
                // if (this.checkPopstateHostlistener && this.articles.length < 3) {
                //   this.service.getArticle(data.slugsByTag, false);
                // }
                // if (this.checkPopState) {
                //   setTimeout(() => {
                //     this.viewportScroller.scrollToPosition(this.scrollPosition);
                //   }, 1);
                // }
                if (this.checkPopstateHostlistener && this.articles.length < 3) {
                  this.service.getArticle(this.nextSlugs[0].slug, false);
                }
                if (this.checkPopState) {
                  setTimeout(() => {
                    this.viewportScroller.scrollToPosition(this.scrollPosition);
                  }, 1);
                }
              }));
            } else if (this.articles.length === 2) {
              this.articles[1].nextSlug = this.nextSlugs[1].slug;
            }

          }));


        }
      }));
  }

}
