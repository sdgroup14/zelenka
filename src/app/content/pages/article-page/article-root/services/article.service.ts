import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {AppAuthService} from '../../../../../services/app-auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonService} from '../../../../../services/common.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public article = new BehaviorSubject(null);

  change(data): void {
    this.article.next(data);
  }

  get(): Observable<any> {
    return this.article.asObservable();
  }

  constructor(private http: HttpClient, private common: CommonService, private route: ActivatedRoute, private router: Router, private auth: AppAuthService) {
  }

  getArticle(slug, init) {
    const param = this.route.snapshot.queryParamMap.get('test_result');
    const query = param ? '?test_result=' + param : '';
    return this.http.get(environment.apiUrl + '/article/' + slug + query, {}).subscribe(resp => {
      this.change(resp);
    }, () => {
      if (init) {
        this.common.url.next(this.router.url);
        this.router.navigate(['/404']);
      }
    });
  }

  getSimilar(article, tag, exists) {
    return this.http.get(environment.apiUrl + '/get-similar?article_id=' + article + '&tag_id=' + tag + '&exist_ids=' + exists, {});
  }

  getNext(article_id, tag_id, exists) {
    return this.http.get(environment.apiUrl + '/get-next/?article_id=' + article_id + '&tag_id=' + tag_id + '&exist_ids=' + exists, {});
  }


  getPoll(id) {
    return this.http.get(environment.apiUrl + '/poll/' + id, {});
  }

  getTest(id) {
    return this.http.get(environment.apiUrl + '/test/' + id, {});
  }

  getComments(id) {
    return this.http.get(environment.apiUrl + '/comments/' + id, {});
  }


  saveComment(data) {
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json').set('Authorization', this.auth.currentUserValue.token)
    };

    const apiData = new FormData();
    for (const option in data) {
      apiData.append(option, data[option]);
    }
    return this.http.post(environment.apiUrl + '/comments', apiData, apiOptions);
  }

  savePoll(id, answer) {
    const apiOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
    return this.http.post(environment.apiUrl + '/poll/' + id, {answer}, apiOptions);
  }

  saveTest(test_id, answers) {
    const apiOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json')
    };
    return this.http.post(environment.apiUrl + '/test', {test_id, answers}, apiOptions);
  }


}
