import {Component, OnDestroy, OnInit} from '@angular/core';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {AppAuthService} from '../../../../services/app-auth.service';
import {AccountPageService} from '../services/account-page.service';
import {Subscription} from 'rxjs';
import {CookiesService} from '@ngx-utils/cookies';
import {CommonService} from '../../../../services/common.service';
import {filter} from 'rxjs/operators';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {ViewportScroller} from '@angular/common';

@Component({
  selector: 'app-account-root',
  templateUrl: './account-root.component.html',
  styleUrls: ['./account-root.component.scss']
})
export class AccountRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  checkProvider = null;
  userTop: any = {};
  userBottom: any = {
    tags: {
      type: 0,
      ids: [98, 102, 105]
    },
    authors: {
      type: 1,
      ids: [11, 12]
    }
  };
  scrollPosition;
  topError = '';
  currentTab = {
    type: 0,
    text: 'Без подписки',
    selected: true
  };
  tagsDropdownList: any = [];
  choosenTags: any = [];

  authorsDropdownList: any = [];
  choosenAuthors: any = [];


  tabs = [
    {
      type: 0,
      text: 'Без подписки',
      selected: true
    },
    {
      type: 2,
      text: 'на всi',
      selected: false
    },
    {
      type: 1,
      text: 'вибiрково',
      selected: false
    }
  ];

  getPhoto(e) {
    const file = e.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = ['jpg', 'jpeg'].filter(format => {
        return format === fileExtension;
      });
      if (checkFormat.length) {
        const reader = new FileReader();
        const that = this;
        reader.onload = (_e: any) => {
          const img = new Image();
          img.src = _e.target.result;
          img.onload = function () {
            that.userTop.photo = file;
          };
        };
        reader.readAsDataURL(file);
      } else {
      }
    }
  }

  saveTop() {
    this.subscriptions.add(this.service.saveTop(this.userTop).subscribe((resp: any) => {
      this.common.notif.next(resp.message);
      const token: any = this.auth.currentUserValue;
      token.user.avatar = resp.user.avatar;
      this.cookie.put('appToken', JSON.stringify(token), {
        path: '/'
      });
      this.auth.currentUserSubject.next(token);
    }, eroor => {
      // console.log(eroor);
    }));
  }

  saveBottom() {
    const tags = [];
    const authors = [];
    this.choosenTags.map(tag => {
      tags.push(tag.id);
    });
    this.choosenAuthors.map(author => {
      authors.push(author.id);
    });
    this.userBottom.tags.ids = tags;
    this.userBottom.authors.ids = authors;
    this.subscriptions.add(this.service.saveBottom(this.userBottom).subscribe((resp: any) => {
      // console.log(resp);
      this.common.notif.next(resp.message);
    }, eroor => {
      // console.log(eroor);
    }));
  }


  changeTab(tab) {
    tab.selected = true;
    this.currentTab = tab;
    this.userBottom.tags.type = tab.type;
    this.tabs.map(_tab => {
      if (tab.type !== _tab.type) {
        _tab.selected = false;
      }
    });
  }

  tagAdd(tag, inp) {
    this.choosenTags.push(tag);
    this.tagsDropdownList = [];
    inp.value = '';
  }

  authorAdd(author, inp) {
    this.choosenAuthors.push(author);
    this.authorsDropdownList = [];
    inp.value = '';
  }


  tagsHandler(e) {
    this.service.getTags(e.target.value);
  }

  authorsHandler(e) {
    this.service.getAuthors(e.target.value);
  }


  deleteChoosenItem(item, arr) {
    const index = arr.indexOf(item);
    arr.splice(index, 1);
  }

  constructor(private preloader: PreloaderService,
              private cookie: CookiesService,
              private common: CommonService,
              public auth: AppAuthService,
              private service: AccountPageService,
              route: ActivatedRoute,
              private router: Router,
              private viewportScroller: ViewportScroller
  ) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.preloader.show();
  }

  ngOnInit() {


    this.subscriptions.add(this.auth.getUser().subscribe((data: any) => {
      this.userTop = {
        photo: data.user.photo,
        name: data.user.name,
      };
      this.choosenAuthors = data.authors;
      this.choosenTags = data.tags;
      if (data.hasOwnProperty('subs')) {
        this.tabs.map(tab => {
          if (tab.type === data.subs.tag_type) {
            this.currentTab = tab;
            tab.selected = true;
          } else {
            tab.selected = false;
          }
        });
        this.userBottom = {
          tags: {
            type: data.subs.tag_type,
            ids: []
          },
          authors: {
            type: 1,
            ids: []
          }
        };
      }

      this.checkProvider = data.user.provider;

      this.preloader.hide();
      setTimeout(() => {
        this.viewportScroller.scrollToPosition(this.scrollPosition);
      }, 10);
    }));

    this.service.tags.subscribe(resp => {
      this.tagsDropdownList = resp;
    });

    this.service.authors.subscribe(resp => {
      this.authorsDropdownList = resp;
    });


  }

}
