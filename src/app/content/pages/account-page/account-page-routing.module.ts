import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccountRootComponent} from './account-root/account-root.component';

const routes: Routes = [
  {
    path: '',
    component: AccountRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPageRoutingModule { }
