import { NgModule } from '@angular/core';

import { AccountPageRoutingModule } from './account-page-routing.module';
import {AccountRootComponent} from './account-root/account-root.component';
import {SharedModule} from '../../../modules/shared.module';
import {AccountPageService} from './services/account-page.service';
import {FormsModule} from '@angular/forms';
import {ImgPipe} from '../../../pipes/img.pipe';

@NgModule({
  declarations: [AccountRootComponent, ImgPipe],
  imports: [
    SharedModule,
    FormsModule,
    AccountPageRoutingModule
  ],
  providers: [AccountPageService]
})
export class AccountPageModule { }
