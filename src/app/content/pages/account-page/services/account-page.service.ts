import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {AppAuthService} from '../../../../services/app-auth.service';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountPageService {
  public tags = new Subject();
  public authors = new Subject();

  constructor(private http: HttpClient, private auth: AppAuthService) {
  }

  saveTop(data) {
    const token = this.auth.currentUserValue.token;

    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json').set('Authorization', token)
    };

    const apiData = new FormData();

    for (const option in data) {
      if (option === 'photo' && (typeof data[option]) === 'string') {

      } else {
        if (data[option] && data[option] !== '') {
          apiData.append(option, data[option]);
        }
      }
    }

    return this.http.post(environment.apiUrl + '/profile', apiData, apiOptions);
  }

  saveBottom(subs) {
    const token = this.auth.currentUserValue.token;

    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json').set('Authorization', token)
    };

    return this.http.post(environment.apiUrl + '/profile/subs', {subs}, apiOptions);
  }

  getTags(search) {
    return this.http.get(environment.apiUrl + '/search-main-tags?search=' + search, {}).subscribe(resp => {
      this.tags.next(resp);
    });
  }

  getAuthors(search) {
    return this.http.get(environment.apiUrl + '/search-authors?search=' + search, {}).subscribe(resp => {
      this.authors.next(resp);
    });
  }


}
