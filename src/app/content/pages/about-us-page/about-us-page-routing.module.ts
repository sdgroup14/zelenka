import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutUsRootComponent} from './about-us-root/about-us-root.component';

const routes: Routes = [
  {
    path: '',
    component: AboutUsRootComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsPageRoutingModule { }
