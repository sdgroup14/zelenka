import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {AboutService} from '../services/about.service';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {ViewportScroller} from '@angular/common';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-about-us-root',
  templateUrl: './about-us-root.component.html',
  styleUrls: ['./about-us-root.component.scss']
})
export class AboutUsRootComponent implements OnInit, OnDestroy {

  private subscriptions = new Subscription();
  scrollPosition;

  constructor(private service: AboutService, private preloader: PreloaderService,
              route: ActivatedRoute, private router: Router, private viewportScroller: ViewportScroller) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  about: any = {};
  team: any = [];

  ngOnInit() {
    this.subscriptions.add(this.service.get().subscribe((resp: any) => {
      this.about = resp.about;
      this.team = resp.command;
      this.preloader.hide();
      setTimeout(() => {
        this.viewportScroller.scrollToPosition(this.scrollPosition);
      }, 10);
    }));
  }

  ngOnDestroy() {
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

}
