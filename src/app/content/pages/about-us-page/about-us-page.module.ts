import { NgModule } from '@angular/core';

import { AboutUsPageRoutingModule } from './about-us-page-routing.module';
import {AboutUsRootComponent} from './about-us-root/about-us-root.component';
import {SharedModule} from '../../../modules/shared.module';
import {AboutService} from './services/about.service';

@NgModule({
  declarations: [AboutUsRootComponent],
  imports: [
    SharedModule,
    AboutUsPageRoutingModule
  ],
  providers: [AboutService]
})
export class AboutUsPageModule { }
