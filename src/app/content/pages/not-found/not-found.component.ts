import {Component, Inject, OnDestroy, OnInit, Optional, PLATFORM_ID} from '@angular/core';
import {isPlatformServer} from '@angular/common';
import {PreloaderService} from '../../templates/preloader/preloader.service';
import {CommonService} from '../../../services/common.service';
import {Location} from '@angular/common';


@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit, OnDestroy {

  constructor(@Inject(PLATFORM_ID) private platformId: Object, private preloader: PreloaderService, private common: CommonService, private location: Location,
              @Optional() @Inject('RESPONSE') private response: any) {
  }

  ngOnDestroy() {
    this.preloader.show();
  }

  ngOnInit() {
    this.common.url.subscribe(url => {
      if (url) {
        this.location.go(url);
      }
    });
    this.preloader.hide();

    if (isPlatformServer(this.platformId)) {
      this.response.statusCode = 404;
    }
  }
}
