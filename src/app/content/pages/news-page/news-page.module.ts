import {NgModule} from '@angular/core';

import {NewsPageRoutingModule} from './news-page-routing.module';
import {NewsListComponent} from './news-list/news-list.component';
import {SharedModule} from '../../../modules/shared.module';
import {NewsListService} from './services/news-list.service';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {LazyLoadImageModule} from 'ng-lazyload-image';

@NgModule({
  declarations: [NewsListComponent],
  imports: [
    SharedModule,
    NewsPageRoutingModule,
    InfiniteScrollModule,
    LazyLoadImageModule
  ],
  providers: [NewsListService]
})
export class NewsPageModule {
}
