import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsListService {

  public news = new Subject();


  constructor(private http: HttpClient) {
  }

  getApiNews(route, page, check) {
    let query = '';
    if (check > 1) query = '&from_zero=' + check;
    return this.http.get(environment.apiUrl + '/news?page=' + page + query, {});
  }

  getApiNewsNext(route, page) {
    return this.http.get(environment.apiUrl + '/news?page=' + page, {}).subscribe(resp => {
      this.news.next(resp);
    });
  }
}
