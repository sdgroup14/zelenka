import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppAuthService} from '../../../services/app-auth.service';
import {PlatformService} from '../../../services/platform.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit {

  constructor(private platform: PlatformService, private route: ActivatedRoute, private router: Router, private appAuth: AppAuthService) {
    if (platform.check()) {
      this.route.queryParams.subscribe(params => {
        if (params.oauth_token) {
          this.appAuth.loginWithSocial(params.oauth_token, 'twitter', params.oauth_verifier);
          return;
        } else {
          this.appAuth.loginWithSocial(
            this.router.url.split('#')[1].split('&')[0].split('=')[1],
            localStorage.getItem('authProvider'));
        }
      });
    }
  }

  ngOnInit() {
  }

}
