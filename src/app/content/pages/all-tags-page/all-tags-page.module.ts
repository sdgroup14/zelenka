import {NgModule} from '@angular/core';

import {AllTagsPageRoutingModule} from './all-tags-page-routing.module';
import {TagsRootComponent} from './tags-root/tags-root.component';
import {SharedModule} from '../../../modules/shared.module';
import {AllTagsService} from './services/all-tags.service';

@NgModule({
  declarations: [TagsRootComponent],
  imports: [
    SharedModule,
    AllTagsPageRoutingModule
  ],
  providers: [AllTagsService]
})
export class AllTagsPageModule {
}
