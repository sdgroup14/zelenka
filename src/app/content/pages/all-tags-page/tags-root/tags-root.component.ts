import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonService} from '../../../../services/common.service';
import {Subscription} from 'rxjs';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {AllTagsService} from '../services/all-tags.service';
import {filter} from 'rxjs/operators';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {ViewportScroller} from '@angular/common';

@Component({
  selector: 'app-tags-root',
  templateUrl: './tags-root.component.html',
  styleUrls: ['./tags-root.component.scss']
})
export class TagsRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  tagsMain: any = [];
  tagsAdditional: any = [];
  scrollPosition;

  constructor(public _common: CommonService, private preloader: PreloaderService, private service: AllTagsService,
              route: ActivatedRoute, private router: Router, private viewportScroller: ViewportScroller) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  ngOnDestroy() {
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    let idx = 0;
    this.tagsMain.map((tag: any) => {
      idx++;
      tag.size = idx;
      if (idx === 5) {
        idx = 0;
      }
    });
    this.tagsAdditional.map((tag: any) => {
      idx++;
      tag.size = idx;
      if (idx === 5) {
        idx = 0;
      }
    });

    this.subscriptions.add(this.service.get().subscribe((resp: any) => {
      resp.main.map((tag: any) => {
        idx++;
        tag.size = idx;
        if (idx === 5) {
          idx = 0;
        }
      });
      resp.additional.map((tag: any) => {
        idx++;
        tag.size = idx;
        if (idx === 5) {
          idx = 0;
        }
      });
      this.tagsMain = resp.main;
      this.tagsAdditional = resp.additional;
      this.preloader.hide();
      setTimeout(() => {
        this.viewportScroller.scrollToPosition(this.scrollPosition);
      }, 10);
    }));
  }
}
