import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TagsRootComponent} from './tags-root/tags-root.component';

const routes: Routes = [
  {
    path: '',
    component: TagsRootComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllTagsPageRoutingModule { }
