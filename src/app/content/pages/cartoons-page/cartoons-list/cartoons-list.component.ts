import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {CartoonService} from '../services/cartoon.service';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {GalleryService} from '../../../templates/gallery/gallery.service';
import {MetaService} from '../../../../services/meta.service';
import {CommonService} from '../../../../services/common.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-cartoons-list',
  templateUrl: './cartoons-list.component.html',
  styleUrls: ['./cartoons-list.component.scss']
})
export class CartoonsListComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  data: any = [];
  pagination: any = {
    currentPage: 1,
    page: 1,
    lastPage: null
  };

  constructor(private service: CartoonService,
              private preloader: PreloaderService,
              public _common: CommonService,
              private sMeta: MetaService,
              private translate: TranslateService,
              public _gallery: GalleryService) {
  }

  galleryHandler() {
    // console.log('aaaaaaaaaaaaaaaaa');
    this._gallery.changeCheck('/caricatures');
  }

  ngOnDestroy() {

    this.preloader.show();
    this.service.changeCaricatures(null);
    this.subscriptions.unsubscribe();
  }

  changePage() {
    this.service.getApiCaricatures(this.pagination.page);
  }

  ngOnInit() {
    this.changePage();
    this.subscriptions.add(
      // this.route.params.subscribe(params => {
      // if (params['slug']) {
      // this.subscriptions.add(
      this.service.getCaricatures().subscribe((resp: any) => {
        if (resp) {
          // console.log(window.location)
          this.translate.stream(['caricatures.metaTitle', 'caricatures.metaKeywords']).subscribe(data => {
            this.sMeta.setMeta(
              data['caricatures.metaTitle'] + ' | iod.media',
              data['caricatures.metaTitle'] + ' | iod.media',
              '',
              data['caricatures.metaKeywords'],
              'https://static1.iod.media/storage/seo-covers/share_cover.jpg',
              this._common.langPath() + '/caricatures',
              'default'
            );
          });

          this.data = this.data.concat(resp.data);
          this.pagination.lastPage = resp.last_page;
          this.pagination.currentPage = resp.current_page;
          if (+this.pagination.page !== +this.pagination.lastPage) {
            this.pagination.page++;
          }
          this.preloader.hide();
        }

        // console.log(resp);
        // this.pagination.page = resp.next;
        // this.pagination.prev = resp.prev;
        // this.tag = resp.tag;
        // this.articles = resp.articles.data;
      })
      // );
      // } else {
      //
      // }
      // })
    );
  }

}
