import { NgModule } from '@angular/core';

import { CartoonsPageRoutingModule } from './cartoons-page-routing.module';
import {CartoonsListComponent} from './cartoons-list/cartoons-list.component';
import {SharedModule} from '../../../modules/shared.module';
import {CartoonService} from './services/cartoon.service';
import {LazyLoadImageModule} from 'ng-lazyload-image';

@NgModule({
  declarations: [CartoonsListComponent],
  imports: [
    SharedModule,
    CartoonsPageRoutingModule,
    LazyLoadImageModule
  ],
  providers: [CartoonService]
})
export class CartoonsPageModule { }
