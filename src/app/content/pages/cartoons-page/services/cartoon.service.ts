import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartoonService {
  public caricatures = new BehaviorSubject(null);

  changeCaricatures(data): void {
    this.caricatures.next(data);
  }

  getCaricatures(): Observable<any> {
    return this.caricatures.asObservable();
  }

  constructor(private http: HttpClient) {
  }

  getApiCaricatures(page) {
    return this.http.get(environment.apiUrl + '/caricatures?page=' + page, {}).subscribe(resp => {
      this.changeCaricatures(resp);
    });
  }
}
