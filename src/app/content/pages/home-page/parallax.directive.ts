import {Directive, ElementRef, HostListener} from '@angular/core';
import {PlatformService} from '../../../services/platform.service';

@Directive({
  selector: '[appParallax]'
})

export class ParallaxDirective {
  startScroll = 0;
  endScroll = 0;
  initialTop = 0;

  wrapMargin = 0;

  constructor(private eleRef: ElementRef, private platform: PlatformService) {
    if (this.platform.check()) {
      this.initialTop = this.eleRef.nativeElement.getBoundingClientRect().top;
    }
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const elTop = this.eleRef.nativeElement.getBoundingClientRect().top;
    const elHeight = this.eleRef.nativeElement.offsetHeight;
    const imgHeight = this.eleRef.nativeElement.children[0].height;
    const imgDelta = imgHeight - elHeight;
    const WW = window.innerWidth;
    const WH = window.innerHeight;
    let progress = 0;
    const distanceDelta = elTop - WH;
    if (!this.wrapMargin) {
      if (WW > WH) {
        this.wrapMargin = (elHeight * .2) * 2;
      } else {
        this.wrapMargin = imgDelta;
      }
    }
    if (distanceDelta <= 0) {
      if (!this.endScroll) {
        this.startScroll = window.scrollY;
        this.endScroll = window.scrollY + WH + elHeight;
      }

      if (window.scrollY < this.endScroll) {
        progress = -distanceDelta / (WH + elHeight) * 100;
        if (WW > WH) {
          this.wrapMargin = (elHeight * .2) * 2;
          this.eleRef.nativeElement.style.marginTop = this.wrapMargin + 'px';
          this.eleRef.nativeElement.children[0].style.top = '50%';
          this.eleRef.nativeElement.style.transform = 'translateY(-' + progress * this.wrapMargin / 100 + 'px)';
        } else {
          this.wrapMargin = imgDelta;
          this.eleRef.nativeElement.children[0].style.top = 0;
          this.eleRef.nativeElement.style.marginTop = 0;
          this.eleRef.nativeElement.children[0].style.transform = 'translateY(-' + progress * this.wrapMargin / 100 + 'px)';
        }
      }
    }
  }
}
