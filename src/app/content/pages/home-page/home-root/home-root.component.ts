import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Subscription, zip} from 'rxjs';
import {HomeService} from '../services/home.service';
import {ActivatedRoute, Router, Scroll} from '@angular/router';
import {PreloaderService} from '../../../templates/preloader/preloader.service';
import {GalleryService} from '../../../templates/gallery/gallery.service';
import {PlatformService} from '../../../../services/platform.service';
import {MetaService} from '../../../../services/meta.service';
import {CommonService} from '../../../../services/common.service';
import {TranslateService} from '@ngx-translate/core';
import {filter} from 'rxjs/operators';
import {Location, ViewportScroller} from '@angular/common';
import {ImgService} from '../../../../services/img.service';

declare var pannellum: any;

@Component({
  selector: 'app-home-root',
  templateUrl: './home-root.component.html',
  styleUrls: ['./home-root.component.scss']
})
export class HomeRootComponent implements OnInit, OnDestroy, AfterViewInit {
  scrollPosition;
  page = 0;

  constructor(
    private service: HomeService,
    public img_service: ImgService,
    public _common: CommonService,
    private _gallery: GalleryService,
    private translate: TranslateService,
    public platform: PlatformService,
    private preloader: PreloaderService,
    private sMeta: MetaService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller,
    private rerender: Renderer2
  ) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  @ViewChild('boxEl', { static: true }) boxEl: ElementRef;

  @ViewChild('el', { static: false }) el: ElementRef;
  _360: any;
  initPage = false;

  private subscriptions = new Subscription();
  floors = [];
  isTicker;
  tickers: any = [];
  color = '#ffffff';
  tint = '#1C1D1F';
  LastIdApi: any = true;
  player: YT.Player;
  id = 'py8qdeyCxZg';

  attachment = false;
  lastPosition: any;
  position: any;
  difference: any;
  lastPos: any = [];

  textInterval: any;

  tickerProps: any = {
    ticker_back: '#1C1D1F',
    ticker_text: '#1C1D1F'
  };

  playYtb(floor) {
    floor.isPlay = true;
    floor.player.playVideo();
  }

  savePlayer(player, floor) {
    floor.player = player;
  }

  imgOnLoad(item, img) {
    this.rerender.addClass(img, '__hide');
  }


  getArticleUrl(article, event) {
    event.preventDefault();
    if (event.target.classList[0] !== 'tags_item' && event.target.classList[0] !== 'playBtn') {
      if (article.is_link) {
        window.open(article.redirect_url, '_blank');
      } else {
        this.router.navigate([this._common.langPath() + '/article', article.slug]);
      }
    }
  }

  changePage() {
    this.page++;
    let url = this.router.url;
    if (url.indexOf('?') > 0) {
      url = url.split('?')[0];
    }
    this.location.replaceState(url, 'page=' + this.page, history.state);
    this.service.getApiNews(this.LastIdApi);
  }

  tagMouseenter(tags) {
    tags.map(_tag => {
      _tag._hover = true;
    });
  }

  tagMouseleave(tags) {
    tags.map(tag => {
      tag._hover = false;
    });
  }

  mouseDown(e) {
    this.lastPosition = [e.clientX, e.clientY];
    this.lastPos = [e.clientX, e.clientY];
  }


  @HostListener('mousedown') onDown() {
    this.attachment = true;
  }

  @HostListener('mouseup') onUp() {
    this.attachment = false;
  }

  mouseMove(e, t) {
    if (this.attachment) {
      this.position = [e.clientX, e.clientY];
      this.difference = [(this.position[0] - this.lastPosition[0]), (this.position[1] - this.lastPosition[1])];
      t.scrollLeft = t.scrollLeft - this.difference[0];
      t.scrollTop = t.scrollTop - this.difference[1];
      this.lastPosition = [e.clientX, e.clientY];
    }
  }

  upTextHandler(article, elem) {
    article.offsetText = 0;
    const render = () => {
      if (elem.offsetHeight < article.offsetText) {
        cancelAnimationFrame(this.textInterval);
        return;
      }
      article.offsetText += 1.3;
      this.textInterval = requestAnimationFrame(render);
    };
    requestAnimationFrame(render);
  }

  clearTextHandler() {
    cancelAnimationFrame(this.textInterval);
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.preloader.show();
  }


  galleryHandler(e, slug) {
    e.preventDefault();
    if (this.lastPos[0] !== e.clientX && this.lastPos[1] !== e.clientY) {
    } else {
      this.router.navigate([this._common.langPath() + '/caricature', slug]);
    }
    this._gallery.changeCheck(this._common.langPath() + '/');
  }

  galleryHandlerFull() {
    this._gallery.changeCheck(this._common.langPath() + '/');
  }


  rotate360(direction) {
    if (!direction) {
      this._360.setYaw(this._360.getYaw() - 45);
    } else {
      this._360.setYaw(this._360.getYaw() + 45);
    }

  }

  ngAfterViewInit() {
  }

  ngOnInit() {

    this.translate.stream(['home.metaDesc', 'home.metaKeywords']).subscribe(data => {
      this.sMeta.setMeta(
        'iod.media',
        'iod.media',
        data['home.metaDesc'],
        data['home.metaKeywords'],
        'https://static1.iod.media/storage/seo-covers/share_cover.jpg',
        this._common.langPath(),
        'main'
      );
    });
    this.subscriptions.add(zip(this.route.queryParams, this._common.backForwardState).subscribe(
      params => {
        let page = null;
        if (params[0]['page']) {
          page = params[0]['page'];
          this.page = page;
        }
        this.subscriptions.add(
          this.service.getArticles(page).subscribe((resp: any) => {
            this.floors = resp.conf;
            this.isTicker = resp.is_ticker;
            this.tickers = resp.ticker_data;
            this.tickerProps.ticker_back = resp.ticker_back;
            this.tickerProps.ticker_text = resp.ticker_text;
            this.LastIdApi = resp.last_publish;
            this.color = resp.color === '#1C1D1F' ? '#ffffff' : resp.color;
            this.tint = resp.color;

            if (this.platform.check()) {
              setTimeout(() => {
                const _360 = this.floors.filter(floor => {
                  return floor.type === 10;
                })[0];
                if (_360) {
                  this._360 = pannellum.viewer(this.el.nativeElement, {
                    type: 'equirectangular',
                    panorama: _360.data.image,
                    mouseZoom: false,
                    compass: false,
                    autoLoad: true,
                    // orientationOnByDefault: true
                  });
                  this._360.startAutoRotate(-1);
                }
              }, 1);
              this.preloader.hide();
              setTimeout(() => {
                this.viewportScroller.scrollToPosition(this.scrollPosition);
              }, 10);
            }
          })
        );
      }
    ));

    this.subscriptions.add(
      this.service.getNews().subscribe(resp => {
        this.floors = this.floors.concat(resp.conf);
        this.LastIdApi = resp.last_publish;
      })
    );
    if (this.platform.check()) {
      this.initPage = true;
    }
  }

}
