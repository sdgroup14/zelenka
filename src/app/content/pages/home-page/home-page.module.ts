import {NgModule} from '@angular/core';
import {HomePageRoutingModule} from './home-page-routing.module';
import {HomeRootComponent} from './home-root/home-root.component';
import {TickerComponent} from '../../templates/ticker/ticker.component';
import {SharedModule} from '../../../modules/shared.module';
import {HomeService} from './services/home.service';
import {ParallaxDirective} from './parallax.directive';
import {FormsModule} from '@angular/forms';
import {LazyLoadImageModule} from 'ng-lazyload-image';

@NgModule({
  declarations: [HomeRootComponent, TickerComponent, ParallaxDirective],
  imports: [
    HomePageRoutingModule,
    SharedModule,
    FormsModule,
    LazyLoadImageModule
  ],
  providers: [HomeService]
})
export class HomePageModule {
}
