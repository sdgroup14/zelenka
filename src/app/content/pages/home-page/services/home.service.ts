import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable, Subject} from 'rxjs';
import {AppAuthService} from '../../../../services/app-auth.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  public news = new Subject();

  changeNews(data): void {
    this.news.next(data);
  }

  getNews(): Observable<any> {
    return this.news.asObservable();
  }

  constructor(private http: HttpClient, private auth: AppAuthService) {
  }

  saveTicker(text) {
    const token = this.auth.currentUserValue.token;

    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json').set('Authorization', token)
    };
    return this.http.post(environment.apiUrl + '/ticker', {text}, apiOptions);
  }

  getArticles(page?) {
    const query = page ? '?page=' + page : '';
    return this.http.get(environment.apiUrl + '/start-page' + query, {});
  }

  getApiNews(id) {
    return this.http.get(environment.apiUrl + '/start-page/paginate?last_publish=' + id, {}).subscribe(resp => {
      this.changeNews(resp);
    });
  }

}
