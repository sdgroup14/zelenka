import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PreloaderService} from '../../templates/preloader/preloader.service';
import {AboutService} from '../about-us-page/services/about.service';
import {ViewportScroller} from '@angular/common';
import {filter} from 'rxjs/operators';
import {Router, Scroll} from '@angular/router';
import {MetaService} from '../../../services/meta.service';
import {TranslateService} from '@ngx-translate/core';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-ads-page',
  templateUrl: './ads-page.component.html',
  styleUrls: ['./ads-page.component.scss']
})
export class AdsPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  about: any = {};
  scrollPosition;

  constructor(
    private preloader: PreloaderService,
    private service: AboutService,
    private viewportScroller: ViewportScroller,
    private sMeta: MetaService,
    private translate: TranslateService,
    private _common: CommonService,
    private router: Router) {
    this.subscriptions.add(this.router.events.pipe(
      filter(e => e instanceof Scroll)
    ).subscribe(e => {
      if ((e as Scroll).position) {
        this.scrollPosition = (e as Scroll).position;
      } else {
        this.scrollPosition = [0, 0];
      }
    }));
  }

  ngOnDestroy() {
    this.preloader.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.service.get().subscribe((resp: any) => {
      this.about = resp.about;
      this.preloader.hide();
      this.translate.get('ads').subscribe(data => {
        const meta = data.meta;
        this.sMeta.setMeta(
          meta.title,
          meta.title,
          meta.descr,
          data.title,
          'https://static1.iod.media/storage/seo-covers/share_cover.jpg',
          this._common.langPath() + '/ads',
          'default'
        );
      });

      setTimeout(() => {
        this.viewportScroller.scrollToPosition(this.scrollPosition);
      }, 10);
    }));
  }

}
