import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CookiesService} from '@ngx-utils/cookies';

import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { Location, DOCUMENT } from '@angular/common';
import {CommonService} from './services/common.service';
import {PlatformService} from './services/platform.service';

declare var gtag: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  menuIsOpen = false;
  searchIsOpen = false;
  notifIsOpen = false;
  authIsOpen = false;
  notifTxt = '';

  constructor(private translate: TranslateService,
              private common: CommonService,
              private location: Location,
              private cookie: CookiesService,
              public platform: PlatformService,
              @Inject(DOCUMENT) private _document: any,
              private router: Router,
              private route: ActivatedRoute) {
    const lang = this.cookie.get('applang');
    if (lang) {
      this.translate.setDefaultLang(lang);
      this._document.documentElement.lang = lang;
    } else {
      this.router.events.subscribe((event: any) => {
        if (event instanceof NavigationEnd) {
          const routerLang: any = event.url.split('/')[1];
          const _lang = routerLang === 'ru' ? 'ru' : 'uk';
          this.translate.use(_lang);
          this._document.documentElement.lang = _lang;
        }
      });
    }

    if (platform.check()) {
      router.events.subscribe((ev: any) => {
        if (ev instanceof NavigationEnd) {
          if (this.router.url !== '/404') {
            gtag(
              'config',
              'UA-134742516-1',
              {
                page_path: ev.urlAfterRedirects,
              }
            );
          }
        }
      });
    }
  }

  ngOnInit() {
    this.location.subscribe(val => {
      this.common.backForwardState.next(true);
    });
    this.common.notif.subscribe((txt: string) => {
      if (txt) {
        this.notifIsOpen = true;
        this.notifTxt = txt;
      } else {
        this.notifIsOpen = false;
      }
    });
  }
}
