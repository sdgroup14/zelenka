import {NgModule} from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';

import {AppModule} from './app.module';
import {AppComponent} from './app.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ServerCookiesModule, ServerCookiesService} from '@ngx-utils/cookies/server';
import {CookiesService} from '@ngx-utils/cookies';
import {TransferState} from '@angular/platform-browser';
import {TranslateServerLoader} from './services/translate-server-loader.service';

export function translateFactory(transferState: TransferState) {
  return new TranslateServerLoader(transferState);
}

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ModuleMapLoaderModule,
    ServerTransferStateModule,
    ServerCookiesModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [TransferState]
      }
    })
  ],
  bootstrap: [AppComponent],
  providers: [{
    provide: CookiesService,
    useClass: ServerCookiesService
  }]
})
export class AppServerModule {
}
