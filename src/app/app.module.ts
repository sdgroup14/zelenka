import {BrowserModule, TransferState} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './content/templates/header/header.component';
import {FooterComponent} from './content/templates/footer/footer.component';
import {MenuComponent} from './content/templates/menu/menu.component';
import {PlatformService} from './services/platform.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {GalleryComponent} from './content/templates/gallery/gallery.component';
import {PreloaderComponent} from './content/templates/preloader/preloader.component';
import {PreloaderService} from './content/templates/preloader/preloader.service';
import {MenuService} from './content/templates/menu/menu.service';
import {GalleryService} from './content/templates/gallery/gallery.service';
import {TransferHttpCacheModule} from '@nguniversal/common';
import {MetaService} from './services/meta.service';
import {CommonService} from './services/common.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {JwtInterceptor} from './interceptors/http.interceptor';
import {BrowserCookiesModule} from '@ngx-utils/cookies/browser';
import {LoginModalComponent} from './content/templates/login-modal/login-modal.component';
import {
  SocialLoginModule
} from 'angularx-social-login';
import {AppAuthService} from './services/app-auth.service';
import {Shared2Module} from './modules/shared2.module';
import {SearchModalComponent} from './content/templates/search-modal/search-modal.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SearchModalService} from './content/templates/search-modal/search-modal.service';
import {FormsModule} from '@angular/forms';
import {NotifModalComponent} from './content/templates/notif-modal/notif-modal.component';
import {AuthEmailComponent} from './content/pages/auth-email-page/auth-email.component';
import {NotFoundComponent} from './content/pages/not-found/not-found.component';
import {TranslateBrowserLoader} from './services/translate-browser-loader.service';
import {AuthPageComponent} from './content/pages/auth-page/auth-page.component';
import {AdsPageComponent} from './content/pages/ads-page/ads-page.component';
import {TopBannerComponent} from './content/templates/top-banner/top-banner.component';
import {BtnScrollUpComponent} from "./content/templates/btn-scroll-up/btn-scroll-up.component";
import {BtnScrollVisibilityDirective} from "./content/templates/btn-scroll-up/btn-scroll-visibility.directive";
import {_StoreModule} from "./store/store.module";

export function exportTranslateStaticLoader(http: HttpClient, transferState: TransferState) {
  return new TranslateBrowserLoader('/assets/i18n/', '.json', transferState, http);
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    GalleryComponent,
    PreloaderComponent,
    LoginModalComponent,
    SearchModalComponent,
    NotifModalComponent,
    AuthEmailComponent,
    NotFoundComponent,
    AuthPageComponent,
    AdsPageComponent,
    TopBannerComponent,
    BtnScrollUpComponent,
    BtnScrollVisibilityDirective
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'my-app'}),
    AppRoutingModule,
    HttpClientModule,
    TransferHttpCacheModule,
    SocialLoginModule,
    BrowserCookiesModule.forRoot(),
    PerfectScrollbarModule,
    Shared2Module,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: exportTranslateStaticLoader,
        deps: [HttpClient, TransferState]
      }
    }),
    _StoreModule
  ],
  providers: [
    PlatformService,
    PreloaderService,
    MenuService,
    GalleryService,
    MetaService,
    SearchModalService,
    AppAuthService,
    CommonService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
