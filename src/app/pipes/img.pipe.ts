import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'img'
})
export class ImgPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  public transform(url) {
    if (typeof url !== 'string' && url) {
      return this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(url));
    }
    return url;
  }

}
