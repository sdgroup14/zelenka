import {Pipe, PipeTransform} from '@angular/core';
import {CommonService} from '../services/common.service';

@Pipe({
  name: 'tagLink'
})
export class TagLinkPipe implements PipeTransform {

  constructor(private common: CommonService) {
  }

  transform(tag: any): any {
    if (!tag.type) {
      return this.common.langPath() + '/tag/' + tag.slug;
    } else {
      return this.common.langPath() + '/search/tag/' + tag.slug;
    }
  }

}
