import { Pipe, PipeTransform } from '@angular/core';
import {CookiesService} from '@ngx-utils/cookies';
import { Location } from '@angular/common';

@Pipe({
  name: 'langLink'
})
export class LangLinkPipe implements PipeTransform {

  constructor(private cookie: CookiesService, private location: Location) {

  }

  transform(value: any, args?: any): any {
    const path = '';
    const lang = this.cookie.get('applang');
    if (lang) {
      if (lang === 'uk') {
        return value;
      }
      return '/' + lang + value;
    } else {
      const url = this.location.path().split('/')[1];
      return (url === 'ru' ? '/' + url : '') + value;
    }
    return path + value;
  }

}
