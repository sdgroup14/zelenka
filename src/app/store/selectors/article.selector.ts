import {IAppState} from '../state/app.state';
import {createSelector} from '@ngrx/store';
import {IArticleState} from "../state/article.state";


const articleState = (state: IAppState) => state.article;


export const selectFullScreenSlider = createSelector(
  articleState,
  (state: IArticleState) => state.fullScreenSlider
);

export const selectFullScreenSliderIsOpen = createSelector(
  articleState,
  (state: IArticleState) => state.fullScreenSlider.isOpen
);

export const selectFullScreenSliderItems = createSelector(
  articleState,
  (state: IArticleState) => state.fullScreenSlider.slides
);

export const selectFullScreenSliderCaption = createSelector(
  articleState,
  (state: IArticleState) => state.fullScreenSlider.caption
);




