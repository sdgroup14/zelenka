import {Action, createReducer, on} from '@ngrx/store';
import {IArticleState, initialArticleState} from "../state/article.state";
import * as ArticlePagesActions from '../actions/article.actions';

const articleReducers = createReducer(
  initialArticleState,
  //
  on(ArticlePagesActions.showSliderModal, (state, {value, slides, caption}) => ({
    ...state,
    fullScreenSlider: {
      isOpen: value,
      slides,
      caption
    }
  }))
);

//
export function articleReducer(state: IArticleState | undefined, action: Action) {
  return articleReducers(state, action);
}
