import {createAction, props} from '@ngrx/store';

export const showSliderModal = createAction('[Article] Show Slider Modal', props<{ value, slides: any[], caption }>());

