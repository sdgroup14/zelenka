export interface IArticleState {
  fullScreenSlider: {
    isOpen: boolean,
    slides: any[],
    caption: string
  }
}

export const initialArticleState: IArticleState = {
  fullScreenSlider: {
    isOpen: false,
    slides: [],
    caption: ''
  }
};

