import {IArticleState, initialArticleState} from "./article.state";

export interface IAppState {
  article: IArticleState;
}

export const initialAppState: IAppState = {
  article: initialArticleState
};
