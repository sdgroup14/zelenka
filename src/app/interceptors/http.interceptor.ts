import {Inject, Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor, HttpParams,
  HttpRequest
} from '@angular/common/http';
import {CookiesService} from '@ngx-utils/cookies';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {AppAuthService} from '../services/app-auth.service';
import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
import { Location, DOCUMENT } from '@angular/common';
import {environment} from '../../environments/environment';

import {CommonService} from '../services/common.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(
    private cookie: CookiesService,
    private common: CommonService,
    private router: Router,
    private appAuth: AppAuthService,
    private location: Location,
    @Inject(DOCUMENT) private _document: any,
  ) {
  }

  isRefreshingToken = false;

  intercept(request: HttpRequest<any>, next: HttpHandler): any {
    let newParams = new HttpParams({fromString: request.params.toString()});
    let lang = this.cookie.get('applang');

    if (lang) {
      lang = lang === 'uk' ? 'ua' : 'ru';
    } else {
      const checkRU = this.location.path().split('/')[1];
      lang = checkRU === 'ru' ? 'ru' : 'ua';
    }

    newParams = newParams.append('lang', lang);
    const requestClone = request.clone({
      params: newParams
    });

    return next.handle(requestClone).pipe(catchError(err => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          if (request.url === `${environment.apiUrl}/auth/refresh`) {
            this.appAuth.clear();
          }
          return this.handleUnauthorized(request, next);
          return throwError(err);
        } else if (err.status === 400) {
          if (request.url === `${environment.apiUrl}/auth/refresh`) {
            this.appAuth.clear();
          }
          return this.handleUnauthorized(request, next);
        } else if (err.status === 404) {
          const params = {
            url: err.url,
            ref: this._document.referrer,
            route: this.router.url
          };
          // console.log('404', params);
          this.common.debugHttp(params);
        }
        return throwError(err);
      } else {

        return throwError(err);
      }
    }));
  }

  handleUnauthorized(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      return this.appAuth.refreshToken()
        .pipe(switchMap((resp: any) => {
            const result: any = resp;
            this.cookie.put('appToken', JSON.stringify(result), {
              path: '/'
            });
            this.appAuth.currentUserSubject.next(result);

            req = req.clone({
              setHeaders: {
                Authorization: result.token
              }
            });
            return next.handle(req);
          }), catchError(error => {
            return throwError(error);
          }), finalize(() => {
            this.isRefreshingToken = false;
          })
        );
    } else {
      return this.appAuth.currentUserSubject
        .pipe(
          filter(token => token != null),
          take(1),
          switchMap(() => {
            req = req.clone({
              setHeaders: {
                Authorization: this.appAuth.currentUserValue.token
              }
            });
            return next.handle(req);
          })
        );
    }
  }
}




