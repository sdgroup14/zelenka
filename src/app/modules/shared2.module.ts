import { NgModule } from '@angular/core';
import {LangLinkPipe} from '../pipes/lang-link.pipe';
import {TagLinkPipe} from '../pipes/tag-link.pipe';

@NgModule({
  declarations: [LangLinkPipe, TagLinkPipe],
  imports: [
  ],
  exports: [LangLinkPipe, TagLinkPipe]
})
export class Shared2Module {}
