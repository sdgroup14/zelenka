import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import {PlatformService} from '../services/platform.service';
import {MoreBtnComponent} from '../content/templates/more-btn/more-btn.component';
import {DateService} from '../services/date.service';
import {YoutubePlayerModule} from 'ngx-youtube-player';
import {TranslateModule} from '@ngx-translate/core';
import {JwtInterceptor} from '../interceptors/http.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ClickOutsideModule} from 'ng-click-outside';
import {Shared2Module} from './shared2.module';

@NgModule({
  declarations: [MoreBtnComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    YoutubePlayerModule,
    TranslateModule,
    ClickOutsideModule,
    Shared2Module
  ],
  providers: [PlatformService, DateService, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  exports: [CommonModule, PerfectScrollbarModule, MoreBtnComponent, YoutubePlayerModule, TranslateModule, ClickOutsideModule, Shared2Module]
})
export class SharedModule {}
