import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {CookiesService} from '@ngx-utils/cookies';

@Injectable({
  providedIn: 'root'
})
export class LangRuGuard implements CanActivate {
  constructor(private cookie: CookiesService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const lang = this.cookie.get('applang');
    const params = next.queryParams;
    // console.log(next)
    if (lang && lang === 'uk') {
      const url_arr = state.url.split('/');
      url_arr.splice(0, 2);

      if (Object.keys(params).length) {
        url_arr[url_arr.length - 1] = url_arr[url_arr.length - 1].split('?')[0];
      }
      // console.log('decode')
      this.router.navigate(['/' + decodeURI(url_arr.join('/'))], {queryParams: params});
      return false;
    }
    return true;
  }
}
