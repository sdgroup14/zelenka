import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {CookiesService} from '@ngx-utils/cookies';

@Injectable({
  providedIn: 'root'
})
export class LangUaGuard implements CanActivate {
  constructor(private cookie: CookiesService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const lang = this.cookie.get('applang');
    const params = next.queryParams;
    if (lang && lang === 'ru') {
      const url_arr = state.url.split('/');
      url_arr[0] = '/ru';
      if (Object.keys(params).length) {
        url_arr[url_arr.length - 1] = url_arr[url_arr.length - 1].split('?')[0];
      }
      this.router.navigate([decodeURI(url_arr.join('/'))], {queryParams: params});
      return false;
    }
    return true;
  }
}
