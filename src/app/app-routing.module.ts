import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GalleryComponent} from './content/templates/gallery/gallery.component';
import {LangRuGuard} from './guards/lang-ru.guard';
import {LangUaGuard} from './guards/lang-ua.guard';
import {AuthGuard} from './guards/auth.guard';
import {AuthEmailComponent} from './content/pages/auth-email-page/auth-email.component';
import {NotFoundComponent} from './content/pages/not-found/not-found.component';
import {AuthPageComponent} from './content/pages/auth-page/auth-page.component';
import {AdsPageComponent} from './content/pages/ads-page/ads-page.component';

const childrens = [
  {
    path: '',
    loadChildren: () => import('./content/pages/home-page/home-page.module').then(m => m.HomePageModule)
  },
  {
    path: 'ads',
    component: AdsPageComponent
  },
  {
    path: 'article',
    loadChildren: () => import('./content/pages/article-page/article-page.module').then(m => m.ArticlePageModule)
  },
  {
    path: 'tag',
    loadChildren: () => import('./content/pages/tags-page/tags-page.module').then(m => m.TagsPageModule)
  },
  {
    path: 'author',
    loadChildren: () => import('./content/pages/authors-page/authors-page.module').then(m => m.AuthorsPageModule)
  },
  {
    path: 'caricatures',
    loadChildren: () => import('./content/pages/cartoons-page/cartoons-page.module').then(m => m.CartoonsPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./content/pages/search-page/search-page.module').then(m => m.SearchPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./content/pages/about-us-page/about-us-page.module').then(m => m.AboutUsPageModule)
  },
  {
    path: 'tags',
    loadChildren: () => import('./content/pages/all-tags-page/all-tags-page.module').then(m => m.AllTagsPageModule)
  },
  {
    path: 'news',
    loadChildren: () => import('./content/pages/news-page/news-page.module').then(m => m.NewsPageModule)
  },
  {
    path: 'spetsproekty',
    loadChildren: () => import('./content/pages/special-projects-page/special-projects-page.module').then(m => m.SpecialProjectsPageModule)
  },
  {
    path: 'account',
    canActivate: [AuthGuard],
    loadChildren: () => import('./content/pages/account-page/account-page.module').then(m => m.AccountPageModule)
  },
  {
    path: 'auth/email',
    component: AuthEmailComponent
  },
  {
    path: 'caricature/:slug',
    component: GalleryComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

const routes: Routes = [
  {
    path: 'auth',
    component: AuthPageComponent
  },
  {
    path: 'ru',
    canActivate: [LangRuGuard],
    children: childrens
  },
  {
    path: '',
    canActivate: [LangUaGuard],
    children: childrens
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      initialNavigation: 'enabled',
      scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
